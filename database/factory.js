'use strict'

/*
|--------------------------------------------------------------------------
| Factory
|--------------------------------------------------------------------------
|
| Factories are used to define blueprints for database tables or Lucid
| models. Later you can use these blueprints to seed your database
| with dummy data.
|
*/
/*
const user = new User()

user.username = 'virk1'
user.password = 'some-password1'
user.email = 'some-password1'
user.name = 'some-password1'

await user.save()
*/
const Factory = use('Factory')

Factory.blueprint('App/Models/User', (faker) => {

  return {
    fid : 'virk4',
    gid : 'some-password4',
    phone : '0841635291',
    email : 'nontadech4@gmail.com',
    password : 'some-password1' 
  }
})
