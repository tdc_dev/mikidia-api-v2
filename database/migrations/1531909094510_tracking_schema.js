'use strict'

const Schema = use('Schema')

class TrackingSchema extends Schema {
  up () {
    this.create('trackings', (collection) => {
      collection.index('trackings_index', {trackings : 1})
    })
  }

  down () {
    this.drop('trackings')
  }
}

module.exports = TrackingSchema
