'use strict'

const Schema = use('Schema')

class UserSchema extends Schema {
  up () {
    this.create('users', (collection) => {
      collection.index('users_index', {username : 1})
      collection.index('key_index', {key : 1})
      collection.index('fid_index', {fid : 1})
      collection.index('gid_index', {gid : 1})
      collection.index('phone_index', {phone : 1})
      collection.index('email_index', {email : 1})
    })
  }

  down () {
    this.drop('users')
  }
}

module.exports = UserSchema
