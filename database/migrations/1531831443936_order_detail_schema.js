'use strict'

const Schema = use('Schema')

class OrderDetailSchema extends Schema {
  up () {
    this.create('order_details', (collection) => {
      collection.index('order_details_index', {order_details : 1})
    })
  }

  down () {
    this.drop('order_details')
  }
}

module.exports = OrderDetailSchema
