'use strict'

const Schema = use('Schema')

class TokenSchema extends Schema {
  up () {
    this.create('tokens', (collection) => {
      collection.index('token_index', {token : 1})
    })
  }

  down () {
    this.drop('tokens')
  }
}

module.exports = TokenSchema
