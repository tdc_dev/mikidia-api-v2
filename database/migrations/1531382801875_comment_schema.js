'use strict'

const Schema = use('Schema')

class CommentSchema extends Schema {
  up () {
    this.create('comments', (collection) => {
      collection.index('comments_index', {comments : 1})
    })
  }

  down () {
    this.drop('comments')
  }
}

module.exports = CommentSchema
