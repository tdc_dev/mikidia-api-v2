'use strict'

const Schema = use('Schema')

class BannerSchema extends Schema {
  up () {
    this.create('banners', (collection) => {
      collection.index('banners_index', {banners : 1})
    })
  }

  down () {
    this.drop('banners')
  }
}

module.exports = BannerSchema
