'use strict'

const Schema = use('Schema')

class ProfileSchema extends Schema {
  up () {
    this.create('profiles', (collection) => {
      collection.index('profiles_index', {profiles : 1})
    })
  }

  down () {
    this.drop('profiles')
  }
}

module.exports = ProfileSchema
