'use strict'

const Schema = use('Schema')

class OrderSchema extends Schema {
  up () {
    this.create('orders', (collection) => {
      collection.index('orders_index', {orders : 1})
    })
  }

  down () {
    this.drop('orders')
  }
}

module.exports = OrderSchema
