'use strict'

const Schema = use('Schema')

class ChatSocketSchema extends Schema {
  up () {
    this.create('chat_sockets', (collection) => {
      collection.index('chatsockets_index', {chat_sockets : 1})
    })
  }

  down () {
    this.drop('chat_sockets')
  }
}

module.exports = ChatSocketSchema
