'use strict'

const Schema = use('Schema')

class AdvertiseSchema extends Schema {
  up () {
    this.create('advertises', (collection) => {
      collection.index('advertises_index', {advertises : 1})
    })
  }

  down () {
    this.drop('advertises')
  }
}

module.exports = AdvertiseSchema
