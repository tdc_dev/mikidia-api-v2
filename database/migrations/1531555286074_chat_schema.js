'use strict'

const Schema = use('Schema')

class ChatSchema extends Schema {
  up () {
    this.create('chats', (collection) => {
      collection.index('chats_index', {chats : 1})
    })
  }

  down () {
    this.drop('chats')
  }
}

module.exports = ChatSchema
