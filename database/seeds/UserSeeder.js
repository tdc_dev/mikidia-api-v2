'use strict'

/*
|--------------------------------------------------------------------------
| UserSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|

*/

const Chance = require('chance'),
chance = new Chance();
const User = use('App/Models/User')

class UserSeeder {
  async run () {
    const user = new User()
    user.fid = chance.fbid()
    user.gid = chance.guid()
    user.phone = chance.phone({ formatted: false })
    user.email = chance.email()
    user.password = chance.password()
    await user.save()
  }
}

module.exports = UserSeeder
