'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

const Route = use('Route')
const version = 'v1'

Route.get('/', ({ request }) => {
  return { greeting: 'Hello world in JSON 1' }
})

Route.on('/chat').render('chat')

Route.group(() => {
  Route.post('/', 'UserController.createUsername')
  Route.put('me', 'UserController.update')
  Route.delete('me', 'UserController.delete')
  Route.put('login', 'UserController.loginUsername')
  Route.put('facebook', 'UserController.loginFacebook')
  Route.put('google', 'UserController.loginGoogle')
  Route.put('token', 'UserController.getToken')
  Route.post('forgot', 'UserController.emailForgotPassword')
}).prefix('api/'+version+'/users')

Route.group(() => {
  Route.post('me', 'ProfileController.write')
  Route.get('me', 'ProfileController.readMe')
  Route.get('id/:uid', 'ProfileController.read')
  Route.delete('me', 'ProfileController.delete')
}).prefix('api/'+version+'/profile')

Route.group(() => {
  Route.post('me', 'AdvertiseController.create')
  Route.put('me', 'AdvertiseController.update')
  Route.get('me', 'AdvertiseController.readMe')
  Route.get('all', 'AdvertiseController.readAll')
  Route.get('id/:aid', 'AdvertiseController.readOne')
  Route.delete('me', 'AdvertiseController.delete')
}).prefix('api/'+version+'/advertise')

Route.group(() => {
  Route.post('/', 'CommentController.create')
  Route.put('/', 'CommentController.update')
  Route.get(':aid', 'CommentController.read')
  Route.delete('/all', 'CommentController.deleteAll')
  Route.delete('/', 'CommentController.delete')
}).prefix('api/'+version+'/comment')

Route.group(() => {
  Route.get(':uid', 'ChatController.read')
  Route.post('savefile', 'ChatController.savefile')
}).prefix('api/'+version+'/chat')

Route.group(() => {
  Route.post('me', 'OrderController.create')
  Route.put('me', 'OrderController.update')
  Route.get('order/:oid', 'OrderController.read')
  Route.get('me', 'OrderController.readUser')
  Route.delete('me', 'OrderController.delete')
}).prefix('api/'+version+'/order')

Route.group(() => {
  Route.post('me', 'TrackingController.create')
  Route.delete('me', 'TrackingController.delete')
}).prefix('api/'+version+'/tracking')

Route.group(() => {
  Route.post('/', 'BannerController.create')
  Route.put('/', 'BannerController.update')
  Route.put('view', 'BannerController.updateView')
  Route.put('click', 'BannerController.updateClick')
  Route.get('position/:position', 'BannerController.readPosition')
  Route.get('all', 'BannerController.readAll')
  Route.get('id/:bid', 'BannerController.readOne')
  Route.delete('/', 'BannerController.delete')
}).prefix('api/'+version+'/banner')
