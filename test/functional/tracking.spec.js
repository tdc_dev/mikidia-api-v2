'use strict'

const { trait, test } = use('Test/Suite')('Tracking')
const Chance = require('chance'),chance = new Chance()
const Helpers = use('Helpers')

trait('Auth/Client')
trait('Test/ApiClient')

var token1, token2, uid, oid, odid

test('Create username tracking', async ({ client }) => {
  const response = await client
  .post('/api/v1/users')
  .send({
    username: chance.email(),
    password: chance.password()
  })
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status: 'Success'
  })
  token1 = response.body.data.token
})

test('Create username tracking', async ({ client }) => {
  const response = await client
  .post('/api/v1/users')
  .send({
    username: chance.email(),
    password: chance.password()
  })
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status: 'Success'
  })
  token2 = response.body.data.token
  uid = response.body.data.uid
})

test('Create tracking', async ({ client }) => {
  const response = await client
  .post('/api/v1/tracking/me')
  .header('Authorization', 'Bearer '+token1)
  .send({
    uid: uid
  })
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status: 'Success'
  })
})

test('Delete tracking', async ({ client }) => {
  const response = await client
  .delete('/api/v1/tracking/me')
  .header('Authorization', 'Bearer '+token1)
  .send({
    uid: uid
  })
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status: 'Success'
  })
})

test('Delete username tracking', async ({ client }) => {
  const response = await client
  .delete('/api/v1/users/me')
  .header('Authorization', 'Bearer '+token1)
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status : 'Success'
  })
})

test('Delete username tracking', async ({ client }) => {
  const response = await client
  .delete('/api/v1/users/me')
  .header('Authorization', 'Bearer '+token2)
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status : 'Success'
  })
})
