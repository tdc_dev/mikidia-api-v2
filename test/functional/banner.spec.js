'use strict'

const { trait, test } = use('Test/Suite')('Banner')
const Chance = require('chance'),chance = new Chance()
const Helpers = use('Helpers')

trait('Auth/Client')
trait('Test/ApiClient')

const username = chance.email()
const password = chance.password()

var token, bid

test('Create username banner', async ({ client }) => {
  const response = await client
  .post('/api/v1/users')
  .send({
    username: username,
    password: password
  })
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status: 'Success'
  })
  token = response.body.data.token
})

test('Create banner picture is empty', async ({ client }) => {
  const response = await client
  .post('/api/v1/banner')
  .header('Authorization', 'Bearer '+token)
  .field('slot', 1)
  .field('position', 'home_banner')
  .field('start_date', '2018-08-27')
  .field('end_date', '2018-08-27')
  .field('link', 'https://translate.google.co.th/')
  .field('status', 'Active')
  .end()
  response.assertStatus(400)
  response.assertJSONSubset({
    status: 'Error',
    messages: 'Picture is empty'
  })
})

test('Create banner', async ({ client }) => {
  const response = await client
  .post('/api/v1/banner')
  .header('Authorization', 'Bearer '+token)
  .attach('picture', Helpers.appRoot('data/cover-image.jpg'))
  .field('slot', 1)
  .field('position', 'home_banner')
  .field('start_date', '2018-08-27')
  .field('end_date', '2018-08-27')
  .field('link', 'https://translate.google.co.th/')
  .field('status', 'Active')
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status: 'Success'
  })
  bid = response.body.data._id
})

test('Update banner', async ({ client }) => {
  const response = await client
  .put('/api/v1/banner')
  .header('Authorization', 'Bearer '+token)
  .attach('picture', Helpers.appRoot('data/cover-image.jpg'))
  .field('bid', bid)
  .field('slot', 1)
  .field('position', 'home_banner')
  .field('start_date', '2018-08-27')
  .field('end_date', '2018-08-27')
  .field('link', 'https://translate.google.co.th/')
  .field('status', 'Active')
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status: 'Success'
  })
})

test('Update banner ID is empty', async ({ client }) => {
  const response = await client
  .put('/api/v1/banner')
  .header('Authorization', 'Bearer '+token)
  .attach('picture', Helpers.appRoot('data/cover-image.jpg'))
  .field('slot', 1)
  .field('position', 'home_banner')
  .field('start_date', '2018-08-27')
  .field('end_date', '2018-08-27')
  .field('link', 'https://translate.google.co.th/')
  .field('status', 'Active')
  .end()
  response.assertStatus(400)
  response.assertJSONSubset({
    status: 'Error',
    messages: 'Banner ID is empty'
  })
})

test('Update banner picture is empty', async ({ client }) => {
  const response = await client
  .put('/api/v1/banner')
  .header('Authorization', 'Bearer '+token)
  .field('bid', bid)
  .field('slot', 1)
  .field('position', 'home_banner')
  .field('start_date', '2018-08-27')
  .field('end_date', '2018-08-27')
  .field('link', 'https://translate.google.co.th/')
  .field('status', 'Active')
  .end()
  response.assertStatus(400)
  response.assertJSONSubset({
    status: 'Error',
    messages: 'Picture is empty'
  })
})

test('Delete banner', async ({ client }) => {
  const response = await client
  .delete('/api/v1/banner')
  .header('Authorization', 'Bearer '+token)
  .send({
    bid: bid
  })
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status: 'Success'
  })
})

test('Delete username banner', async ({ client }) => {
  const response = await client
  .delete('/api/v1/users/me')
  .header('Authorization', 'Bearer '+token)
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status: 'Success'
  })
})
