'use strict'

const { trait, test } = use('Test/Suite')('Profile')
const Chance = require('chance'),chance = new Chance()
const Helpers = use('Helpers')

trait('Auth/Client')
trait('Test/ApiClient')

const username = chance.email()
const password = chance.password()
var token, uid, gg_token

test('Create username profile', async ({ client }) => {
  const response = await client
  .post('/api/v1/users')
  .send({
    username: username,
    password: password
  })
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status: 'Success'
  })
  token = response.body.data.token
  uid  = response.body.data.uid
})

test('Write profile insert', async ({ client }) => {
  const display_name = chance.word({ length: 10 })
  const phone = chance.phone({ formatted: false })
  const email = chance.email()
  const location = chance.integer({ min: 1, max: 100 })
  const birthday = new Date()
  const response = await client
  .post('/api/v1/profile/me')
  .header('Authorization', 'Bearer '+token)
  .send({
    display_name: display_name,
    phone: phone,
    email: email,
    birthday: birthday,
    location: location
  })
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status: 'Success'
  })
})

test('Write profile update', async ({ client }) => {
  const display_name = chance.word({ length: 10 })
  const phone = chance.phone({ formatted: false })
  const email = chance.email()
  const location = chance.integer({ min: 1, max: 100 })
  const birthday = new Date()
  const response = await client
  .post('/api/v1/profile/me')
  .header('Authorization', 'Bearer '+token)
  .send({
    display_name: display_name,
    phone: phone,
    email: email,
    birthday: birthday,
    location: location
  })
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status: 'Success'
  })
})

test('Write profile picture link', async ({ client }) => {
  const display_name = chance.word({ length: 10 })
  const phone = chance.phone({ formatted: false })
  const email = chance.email()
  const location = chance.integer({ min: 1, max: 100 })
  const birthday = new Date()
  const picture = 'https://scontent.fbkk1-3.fna.fbcdn.net/v/t1.0-1/p160x160/31689104_10216350129082862_5305922762626301952_n.jpg?_nc_cat=0&oh=2fb749b93ac490461536902ec0c528d7&oe=5BEB906B'
  const response = await client
  .post('/api/v1/profile/me')
  .header('Authorization', 'Bearer '+token)
  .send({
    display_name: display_name,
    phone: phone,
    email: email,
    birthday: birthday,
    location: location,
    picture: picture
  })
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status: 'Success'
  })
})

test('Write profile picture', async ({ client }) => {
  const display_name = chance.word({ length: 10 })
  const phone = chance.phone({ formatted: false })
  const email = chance.email()
  const location = chance.integer({ min: 1, max: 100 })
  const birthday = new Date()
  const response = await client
  .post('/api/v1/profile/me')
  .header('Authorization', 'Bearer '+token)
  .attach('picture', Helpers.appRoot('data/cover-image.jpg'))
  .field('display_name', display_name)
  .field('phone', phone)
  .field('email', email)
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status: 'Success'
  })
})

test('Write profile token has expired', async ({ client }) => {
  const display_name = chance.word({ length: 10 })
  const phone = chance.phone({ formatted: false })
  const email = chance.email()
  const location = chance.integer({ min: 1, max: 100 })
  const birthday = new Date()
  const response = await client
  .post('/api/v1/profile/me')
  .header('Authorization', 'Bearer '+chance.word({ length: 10 }))
  .send({
    display_name: display_name,
    phone: phone,
    email: email,
    birthday: birthday,
    location: location
  })
  .end()
  response.assertStatus(400)
  response.assertJSONSubset({
    status: 'Error',
    messages: 'Token has expired'
  })
})

test('Read profile', async ({ client }) => {
  const response = await client
  .get('/api/v1/profile/me')
  .header('Authorization', 'Bearer '+token)
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status: 'Success'
  })
})

test('Read profile token has expired', async ({ client }) => {
  const response = await client
  .get('/api/v1/profile/me')
  .header('Authorization', 'Bearer '+chance.word({ length: 10 }))
  .end()
  response.assertStatus(400)
  response.assertJSONSubset({
    status: 'Error',
    messages: 'Token has expired'
  })
})

test('Delete profile token has expired', async ({ client }) => {
  const response = await client
  .delete('/api/v1/profile/me')
  .header('Authorization', 'Bearer '+chance.word({ length: 10 }))
  .end()
  response.assertStatus(400)
  response.assertJSONSubset({
    status: 'Error',
    messages: 'Token has expired'
  })
})

test('Delete profile', async ({ client }) => {
  const response = await client
  .delete('/api/v1/profile/me')
  .header('Authorization', 'Bearer '+token)
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status: 'Success'
  })
})

test('Delete username profile token has expired', async ({ client }) => {
  const response = await client
  .delete('/api/v1/users/me')
  .header('Authorization', 'Bearer '+chance.word({ length: 10 }))
  .end()
  response.assertStatus(400)
  response.assertJSONSubset({
    status: 'Error',
    messages: 'Token has expired'
  })
})

test('Delete username profile', async ({ client }) => {
  const response = await client
  .delete('/api/v1/users/me')
  .header('Authorization', 'Bearer '+token)
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status: 'Success'
  })
})
