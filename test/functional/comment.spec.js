'use strict'

const { trait, test } = use('Test/Suite')('Comment')
const Chance = require('chance'),chance = new Chance()
const Helpers = use('Helpers')

trait('Auth/Client')
trait('Test/ApiClient')

const username = chance.email()
const password = chance.password()
const title = chance.word()
const detail = chance.paragraph()
const price = chance.integer({ min: 1000, max: 99999 })
const location = chance.integer({ min: 1, max: 100 })
const view = chance.integer({ min: 1000, max: 99999 })
const vote = chance.integer({ min: 1, max: 5 })

var token, aid, cid

test('Create username comment', async ({ client }) => {
  const response = await client
  .post('/api/v1/users')
  .send({
    username: username,
    password: password
  })
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status: 'Success'
  })
  token = response.body.data.token
})

test('Create advertise comment', async ({ client }) => {
  const response = await client
  .post('/api/v1/advertise/me')
  .header('Authorization', 'Bearer '+token)
  .attach('picture[]', Helpers.appRoot('data/cover-image.jpg'))
  .attach('picture[]', Helpers.appRoot('data/cover-image.jpg'))
  .field('title', title)
  .field('detail', detail)
  .field('price', price)
  .field('location', location)
  .field('view', view)
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status: 'Success'
  })
  aid = response.body.data._id
})

test('Create comment Advertise ID is empty', async ({ client }) => {
  const response = await client
  .post('/api/v1/comment')
  .header('Authorization', 'Bearer '+token)
  .send({
    vote: vote,
    detail: detail
  })
  .end()
  response.assertStatus(400)
  response.assertJSONSubset({
    status: 'Error',
    messages: 'Advertise ID is empty'
  })
})

test('Create comment Vote or Comment is empty', async ({ client }) => {
  const response = await client
  .post('/api/v1/comment')
  .header('Authorization', 'Bearer '+token)
  .send({
    aid: aid
  })
  .end()
  response.assertStatus(400)
  response.assertJSONSubset({
    status: 'Error',
    messages: 'Vote or Comment is empty'
  })
})

test('Create comment', async ({ client }) => {
  const response = await client
  .post('/api/v1/comment')
  .header('Authorization', 'Bearer '+token)
  .send({
    aid: aid,
    vote: vote,
    detail: detail
  })
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status: 'Success'
  })
  cid = response.body.data._id
})

test('Update comment ID is empty', async ({ client }) => {
  const response = await client
  .put('/api/v1/comment')
  .header('Authorization', 'Bearer '+token)
  .send({
    vote: vote,
    detail: detail
  })
  .end()
  response.assertStatus(400)
  response.assertJSONSubset({
    status: 'Error',
    messages: 'Comment ID is empty'
  })
})

test('Update comment Vote or Comment is empty', async ({ client }) => {
  const response = await client
  .put('/api/v1/comment')
  .header('Authorization', 'Bearer '+token)
  .send({
    cid: cid
  })
  .end()
  response.assertStatus(400)
  response.assertJSONSubset({
    status: 'Error',
    messages: 'Vote or Comment is empty'
  })
})

test('Update comment', async ({ client }) => {
  const response = await client
  .put('/api/v1/comment')
  .header('Authorization', 'Bearer '+token)
  .send({
    cid: cid,
    vote: vote,
    detail: detail
  })
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status: 'Success'
  })
})

test('Update comment', async ({ client }) => {
  const response = await client
  .put('/api/v1/comment')
  .header('Authorization', 'Bearer '+token)
  .send({
    cid: cid,
    vote: vote,
    detail: detail
  })
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status: 'Success'
  })
})

test('Delete comment', async ({ client }) => {
  const response = await client
  .delete('/api/v1/comment')
  .header('Authorization', 'Bearer '+token)
  .send({
    cid: cid
  })
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status: 'Success'
  })
})

test('Delete advertise comment', async ({ client }) => {
  const response = await client
  .delete('/api/v1/advertise/me')
  .header('Authorization', 'Bearer '+token)
  .send({
    aid: aid
  })
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status: 'Success'
  })
})

test('Delete username comment', async ({ client }) => {
  const response = await client
  .delete('/api/v1/users/me')
  .header('Authorization', 'Bearer '+token)
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status: 'Success'
  })
})
