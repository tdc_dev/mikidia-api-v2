'use strict'

const { trait, test } = use('Test/Suite')('Chat')
const Chance = require('chance'),chance = new Chance()
const Helpers = use('Helpers')

trait('Auth/Client')
trait('Test/ApiClient')

const title = chance.word()
const detail = chance.paragraph()
const price = chance.integer({ min: 1000, max: 99999 })
const location = chance.integer({ min: 1, max: 100 })
const view = chance.integer({ min: 1000, max: 99999 })

var token1, token2, uid, aid

test('Create username chat', async ({ client }) => {
  const response = await client
  .post('/api/v1/users')
  .send({
    username: chance.email(),
    password: chance.password()
  })
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status: 'Success'
  })
  token1 = response.body.data.token
})

test('Create username chat', async ({ client }) => {
  const response = await client
  .post('/api/v1/users')
  .send({
    username: chance.email(),
    password: chance.password()
  })
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status: 'Success'
  })
  token2 = response.body.data.token
  uid = response.body.data.uid
})

test('Read chat profile found user', async ({ client }) => {
  const response = await client
  .get('/api/v1/chat/1b4c1bf9b0796807f5cce5b1')
  .header('Authorization', 'Bearer '+token1)
  .end()
  response.assertStatus(400)
  response.assertJSONSubset({
    status: 'Error',
    messages: 'Profile found user'
  })
})

test('Read chat', async ({ client }) => {
  const response = await client
  .get('/api/v1/chat/'+uid)
  .header('Authorization', 'Bearer '+token1)
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status: 'Success'
  })
})

test('Save file chat', async ({ client }) => {
  const response = await client
  .post('/api/v1/chat/savefile')
  .header('Authorization', 'Bearer '+token1)
  .attach('picture', Helpers.appRoot('data/cover-image.jpg'))
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status: 'Success'
  })
})

test('Save file chat', async ({ client }) => {
  const response = await client
  .post('/api/v1/chat/savefile')
  .header('Authorization', 'Bearer '+token1)
  .end()
  response.assertStatus(400)
  response.assertJSONSubset({
    status: 'Error',
    messages: 'Cat not upload'
  })
})

test('Delete username chat', async ({ client }) => {
  const response = await client
  .delete('/api/v1/users/me')
  .header('Authorization', 'Bearer '+token1)
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status : 'Success'
  })
})

test('Delete username chat', async ({ client }) => {
  const response = await client
  .delete('/api/v1/users/me')
  .header('Authorization', 'Bearer '+token2)
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status : 'Success'
  })
})
