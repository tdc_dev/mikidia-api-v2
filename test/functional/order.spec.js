'use strict'

const { trait, test } = use('Test/Suite')('Order')
const Chance = require('chance'),chance = new Chance()
const Helpers = use('Helpers')

trait('Auth/Client')
trait('Test/ApiClient')

const detail = chance.paragraph()
const price = chance.integer({ min: 1000, max: 99999 })
const location = chance.integer({ min: 1, max: 100 })
const view = chance.integer({ min: 1000, max: 99999 })
const expire = '2018-07-27'

var token1, token2, uid, oid, odid

test('Create username order', async ({ client }) => {
  const response = await client
  .post('/api/v1/users')
  .send({
    username: chance.email(),
    password: chance.password()
  })
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status: 'Success'
  })
  token1 = response.body.data.token
})

test('Create username order', async ({ client }) => {
  const response = await client
  .post('/api/v1/users')
  .send({
    username: chance.email(),
    password: chance.password()
  })
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status: 'Success'
  })
  token2 = response.body.data.token
  uid = response.body.data.uid
})

test('Create order ID saller is empty', async ({ client }) => {
  const response = await client
  .post('/api/v1/order/me')
  .header('Authorization', 'Bearer '+token1)
  .attach('picture[]', Helpers.appRoot('data/cover-image.jpg'))
  .attach('picture[]', Helpers.appRoot('data/cover-image.jpg'))
  .field('detail', detail)
  .field('price', price)
  .field('location', location)
  .field('view', view)
  .field('expire', expire)
  .end()
  response.assertStatus(400)
  response.assertJSONSubset({
    status: 'Error',
    messages: 'ID Saller is empty'
  })
})

test('Create order expire day is empty', async ({ client }) => {
  const response = await client
  .post('/api/v1/order/me')
  .header('Authorization', 'Bearer '+token1)
  .attach('picture[]', Helpers.appRoot('data/cover-image.jpg'))
  .attach('picture[]', Helpers.appRoot('data/cover-image.jpg'))
  .field('detail', detail)
  .field('uid_saller', uid)
  .field('price', price)
  .field('location', location)
  .field('view', view)
  .end()
  response.assertStatus(400)
  response.assertJSONSubset({
    status: 'Error',
    messages: 'Expire day is empty'
  })
})

test('Create order', async ({ client }) => {
  const response = await client
  .post('/api/v1/order/me')
  .header('Authorization', 'Bearer '+token1)
  .attach('picture[]', Helpers.appRoot('data/cover-image.jpg'))
  .attach('picture[]', Helpers.appRoot('data/cover-image.jpg'))
  .field('uid_saller', uid)
  .field('detail', detail)
  .field('price', price)
  .field('location', location)
  .field('view', view)
  .field('expire', expire)
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status: 'Success'
  })
  oid = response.body.data._id
  odid = response.body.data.order_details[0]._id
})

test('Update order', async ({ client }) => {
  const response = await client
  .put('/api/v1/order/me')
  .header('Authorization', 'Bearer '+token1)
  .attach('picture[]', Helpers.appRoot('data/cover-image.jpg'))
  .attach('picture[]', Helpers.appRoot('data/cover-image.jpg'))
  .field('oid', oid)
  .field('uid_saller', uid)
  .field('detail', detail)
  .field('price', price)
  .field('location', location)
  .field('view', view)
  .field('expire', expire)
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status: 'Success'
  })
})

test('Update order detail', async ({ client }) => {
  const response = await client
  .put('/api/v1/order/me')
  .header('Authorization', 'Bearer '+token1)
  .attach('picture[]', Helpers.appRoot('data/cover-image.jpg'))
  .attach('picture[]', Helpers.appRoot('data/cover-image.jpg'))
  .field('oid', oid)
  .field('odid', odid)
  .field('uid_saller', uid)
  .field('detail', detail)
  .field('price', price)
  .field('location', location)
  .field('view', view)
  .field('expire', expire)
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status: 'Success'
  })
})

test('Update order order ID is empty', async ({ client }) => {
  const response = await client
  .put('/api/v1/order/me')
  .header('Authorization', 'Bearer '+token1)
  .attach('picture[]', Helpers.appRoot('data/cover-image.jpg'))
  .attach('picture[]', Helpers.appRoot('data/cover-image.jpg'))
  .field('uid_saller', uid)
  .field('detail', detail)
  .field('price', price)
  .field('location', location)
  .field('view', view)
  .field('expire', expire)
  .end()
  response.assertStatus(400)
  response.assertJSONSubset({
    status: 'Error',
    messages: 'Order ID is empty'
  })
})

test('Update order ID saller is empty', async ({ client }) => {
  const response = await client
  .put('/api/v1/order/me')
  .header('Authorization', 'Bearer '+token1)
  .attach('picture[]', Helpers.appRoot('data/cover-image.jpg'))
  .attach('picture[]', Helpers.appRoot('data/cover-image.jpg'))
  .field('oid', oid)
  .field('detail', detail)
  .field('price', price)
  .field('location', location)
  .field('view', view)
  .field('expire', expire)
  .end()
  response.assertStatus(400)
  response.assertJSONSubset({
    status: 'Error',
    messages: 'ID Saller is empty'
  })
})

test('Delete order', async ({ client }) => {
  const response = await client
  .delete('/api/v1/order/me')
  .header('Authorization', 'Bearer '+token1)
  .send({
    oid: oid
  })
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status: 'Success'
  })
})

test('Update order cat not update', async ({ client }) => {
  const response = await client
  .put('/api/v1/order/me')
  .header('Authorization', 'Bearer '+token1)
  .attach('picture[]', Helpers.appRoot('data/cover-image.jpg'))
  .attach('picture[]', Helpers.appRoot('data/cover-image.jpg'))
  .field('oid', oid)
  .field('odid', odid)
  .field('uid_saller', uid)
  .field('detail', detail)
  .field('price', price)
  .field('location', location)
  .field('view', view)
  .field('expire', expire)
  .end()
  response.assertStatus(400)
  response.assertJSONSubset({
    status: 'Error',
    messages: 'Cat not update'
  })
})

test('Delete username order', async ({ client }) => {
  const response = await client
  .delete('/api/v1/users/me')
  .header('Authorization', 'Bearer '+token1)
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status : 'Success'
  })
})

test('Delete username order', async ({ client }) => {
  const response = await client
  .delete('/api/v1/users/me')
  .header('Authorization', 'Bearer '+token2)
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status : 'Success'
  })
})
