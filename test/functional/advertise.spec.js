'use strict'

const { trait, test } = use('Test/Suite')('Advertise')
const Chance = require('chance'),chance = new Chance()
const Helpers = use('Helpers')

trait('Auth/Client')
trait('Test/ApiClient')

const username = chance.email()
const password = chance.password()
const title = chance.word()
const detail = chance.paragraph()
const price = chance.integer({ min: 1000, max: 99999 })
const location = chance.integer({ min: 1, max: 100 })
const view = chance.integer({ min: 1000, max: 99999 })

var token, aid

test('Create username advertise', async ({ client }) => {
  const response = await client
  .post('/api/v1/users')
  .send({
    username: username,
    password: password
  })
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status: 'Success'
  })
  token = response.body.data.token
})

test('Create advertise title is empty', async ({ client }) => {
  const response = await client
  .post('/api/v1/advertise/me')
  .header('Authorization', 'Bearer '+token)
  .attach('picture[]', Helpers.appRoot('data/cover-image.jpg'))
  .attach('picture[]', Helpers.appRoot('data/cover-image.jpg'))
  .field('detail', detail)
  .field('price', price)
  .field('location', location)
  .field('view', view)
  .end()
  response.assertStatus(400)
  response.assertJSONSubset({
    status: 'Error',
    messages: 'Title is empty'
  })
})

test('Create advertise Picture is empty', async ({ client }) => {
  const response = await client
  .post('/api/v1/advertise/me')
  .header('Authorization', 'Bearer '+token)
  .field('title', title)
  .end()
  response.assertStatus(400)
  response.assertJSONSubset({
    status: 'Error',
    messages: 'Picture is empty'
  })
})

test('Create advertise', async ({ client }) => {
  const response = await client
  .post('/api/v1/advertise/me')
  .header('Authorization', 'Bearer '+token)
  .attach('picture[]', Helpers.appRoot('data/cover-image.jpg'))
  .attach('picture[]', Helpers.appRoot('data/cover-image.jpg'))
  .field('title', title)
  .field('detail', detail)
  .field('price', price)
  .field('location', location)
  .field('view', view)
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status: 'Success'
  })
  aid = response.body.data._id
})

test('Update advertise', async ({ client }) => {
  const response = await client
  .put('/api/v1/advertise/me')
  .header('Authorization', 'Bearer '+token)
  .attach('picture[]', Helpers.appRoot('data/cover-image.jpg'))
  .attach('picture[]', Helpers.appRoot('data/cover-image.jpg'))
  .field('aid', aid)
  .field('title', title)
  .field('detail', detail)
  .field('price', price)
  .field('location', location)
  .field('view', view)
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status: 'Success'
  })
})

test('Update advertise ID is empty', async ({ client }) => {
  const response = await client
  .put('/api/v1/advertise/me')
  .header('Authorization', 'Bearer '+token)
  .attach('picture[]', Helpers.appRoot('data/cover-image.jpg'))
  .attach('picture[]', Helpers.appRoot('data/cover-image.jpg'))
  .field('title', title)
  .field('detail', detail)
  .field('price', price)
  .field('location', location)
  .field('view', view)
  .end()
  response.assertStatus(400)
  response.assertJSONSubset({
    status: 'Error',
    messages: 'Advertise ID is empty'
  })
})

test('Update advertise title is empty', async ({ client }) => {
  const response = await client
  .put('/api/v1/advertise/me')
  .header('Authorization', 'Bearer '+token)
  .attach('picture[]', Helpers.appRoot('data/cover-image.jpg'))
  .attach('picture[]', Helpers.appRoot('data/cover-image.jpg'))
  .field('aid', aid)
  .field('detail', detail)
  .field('price', price)
  .field('location', location)
  .field('view', view)
  .end()
  response.assertStatus(400)
  response.assertJSONSubset({
    status: 'Error',
    messages: 'Title is empty'
  })
})

test('Update advertise picture is empty', async ({ client }) => {
  const response = await client
  .put('/api/v1/advertise/me')
  .header('Authorization', 'Bearer '+token)
  .field('aid', aid)
  .field('title', title)
  .field('detail', detail)
  .field('price', price)
  .field('location', location)
  .field('view', view)
  .end()
  response.assertStatus(400)
  response.assertJSONSubset({
    status: 'Error',
    messages: 'Picture is empty'
  })
})

test('Delete advertise', async ({ client }) => {
  const response = await client
  .delete('/api/v1/advertise/me')
  .header('Authorization', 'Bearer '+token)
  .send({
    aid: aid
  })
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status: 'Success'
  })
})

test('Delete username advertise', async ({ client }) => {
  const response = await client
  .delete('/api/v1/users/me')
  .header('Authorization', 'Bearer '+token)
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status: 'Success'
  })
})
