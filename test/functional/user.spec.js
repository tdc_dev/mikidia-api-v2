'use strict'

const { trait, test, before, beforeEach, after, afterEach } = use('Test/Suite')('User')
const Chance = require('chance'),chance = new Chance()
const fs = require('fs')
const jwt = require('jsonwebtoken')
const Client = require('node-rest-client').Client
const Mail = use('Mail')

trait('Auth/Client')
trait('Test/ApiClient')

const username = chance.email()
const password = chance.password()
const fb_token = 'EAAB0dACRCvoBAOb4eqe9GdYK8DXZCqlbiRMhXwZCDC6l4CarLmSvqrOvVrFOpZBHhurYQ3d53ZC7WAKcYl5XWn9MoFcqClvAIHIbW51HuL1XZCobT4pZAtJkqeNaIoQn1wTgWAMm6pyqfxnFO4vZCy40nhamAWiftHvDn2m6dPRlQZDZD'

var token, key, gg_token

before(async () => {
    var file = 'data/mikidia-8895833a361c.json'
    var data = fs.readFileSync(file)
    data =  JSON.parse(data.toString())
    var cert =  data.private_key
    var token = await jwt.sign({
      iss : data.client_email,
      scope : 'https://www.googleapis.com/auth/plus.login',
      aud : data.auth_uri,
      exp : Math.floor(Date.now() / 1000) + (60 * 60),
      iat : Math.floor(Date.now() / 1000) - 30
    }, cert, { algorithm: 'RS256'})

    var postRequest = async function(url, args, callback) {
      var client = new Client()
      var responseData = {}
      client.post(url, args, function(data) {
        callback(data)
      })
    }
    var args = {
        data : {
          grant_type : "urn:ietf:params:oauth:grant-type:jwt-bearer",
          assertion : token,
        },
        headers : { "Content-Type" : "application/x-www-form-urlencoded" }
    }
    postRequest(data.auth_uri, args, function(data) {
        gg_token = data.access_token
    })
})

test('Create username success', async ({ client }) => {
  const response = await client
  .post('/api/v1/users')
  .send({
    username : username,
    password : password
  }).end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status : 'Success'
  })
})

test('Check email', async ({ client }) => {
  Mail.fake()
  const response = await client
  .post('/api/v1/users/forgot')
  .send({
    email : username
  }).end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status : 'Success'
  })
  Mail.restore()
})

test('Check email not found', async ({ client }) => {
  Mail.fake()
  const response = await client
  .post('/api/v1/users/forgot')
  .send({
    email : chance.email()
  }).end()
  response.assertStatus(400)
  response.assertJSONSubset({
    status : 'Error',
    messages : 'Email not found'
  })
  Mail.restore()
})

test('Check email format invalid', async ({ client }) => {
  Mail.fake()
  const response = await client
  .post('/api/v1/users/forgot')
  .send({
    email : chance.password()
  }).end()
  response.assertStatus(400)
  response.assertJSONSubset({
    status : 'Error',
    messages : 'Email format invalid'
  })
  Mail.restore()
})

test('Create username deplicate', async ({ client }) => {
  const response = await client
  .post('/api/v1/users')
  .send({
    username : username,
    password : password
  }).end()
  response.assertStatus(400)
  response.assertJSONSubset({
    status : 'Error',
    messages : 'Username duplicate'
  })
})

test('Create username is null', async ({ client }) => {
  const response = await client
  .post('/api/v1/users')
  .send({
    username : '',
    password : password
  }).end()
  response.assertStatus(400)
  response.assertJSONSubset({
    status : 'Error',
    messages : 'Username is null'
  })
})

test('Create password is null', async ({ client }) => {
  const response = await client
  .post('/api/v1/users')
  .send({
    username : chance.word({ length: 10 }),
    password : ''
  }).end()
  response.assertStatus(400)
  response.assertJSONSubset({
    status : 'Error',
    messages : 'Password is null'
  })
})

test('Create username format invalid', async ({ client }) => {
  const response = await client
  .post('/api/v1/users')
  .send({
    username : chance.word({ length: 10 }),
    password : password
  }).end()
  response.assertStatus(400)
  response.assertJSONSubset({
    status : 'Error',
    messages : 'Username format invalid'
  })
})

test('Login username', async ({ client }) => {
  const response = await client
  .put('/api/v1/users/login')
  .send({
    username : username,
    password : password
  }).end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status : 'Success'
  })
  token = response.body.data.token
  key  = response.body.data.key
})

test('Login username not success', async ({ client }) => {
  const response = await client
  .put('/api/v1/users/login')
  .send({
    username : chance.word({ length: 10 }),
    password : chance.password()
  }).end()
  response.assertStatus(400)
  response.assertJSONSubset({
    status : 'Error',
    messages : 'User not found'
  })
})

test('Login username is null', async ({ client }) => {
  const response = await client
  .put('/api/v1/users/login')
  .send({
    username : '',
    password : password
  }).end()
  response.assertStatus(400)
  response.assertJSONSubset({
    status : 'Error',
    messages : 'Username is null'
  })
})

test('Login password is null', async ({ client }) => {
  const response = await client
  .put('/api/v1/users/login')
  .send({
    username : username,
    password : ''
  }).end()
  response.assertStatus(400)
  response.assertJSONSubset({
    status : 'Error',
    messages : 'Password is null'
  })
})

test('Check token username', async ({ client }) => {
  const response = await client
  .put('/api/v1/users/token')
  .header('Authorization', 'Bearer '+token)
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status : 'Success'
  })
})

test('Check token username UID', async ({ client }) => {
  const response = await client
  .put('/api/v1/users/token')
  .header('Authorization', 'Bearer '+chance.word({ length: 50 }))
  .send({
    key : key
  })
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status : 'Success'
  })
  token = response.body.data.token
})

test('Check token username UID not token', async ({ client }) => {
  const response = await client
  .put('/api/v1/users/token')
  .send({
    key : key
  })
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status : 'Success'
  })
  token = response.body.data.token
})

test('Delete username token has expired', async ({ client }) => {
  const response = await client
  .delete('/api/v1/users/me')
  .header('Authorization', 'Bearer '+chance.word({ length: 50 }))
  .end()
  response.assertStatus(400)
  response.assertJSONSubset({
    status: 'Error',
    messages: 'Token has expired'
  })
})

test('Delete username', async ({ client }) => {
  const response = await client
  .delete('/api/v1/users/me')
  .header('Authorization', 'Bearer '+token)
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status : 'Success'
  })
})

test('Check token username UID not found', async ({ client }) => {
  const response = await client
  .put('/api/v1/users/token')
  .header('Authorization', 'Bearer '+chance.word({ length: 50 }))
  .send({
    key : key
  })
  .end()
  response.assertStatus(400)
  response.assertJSONSubset({
    status : 'Error',
    messages : 'User not found'
  })
})

test('Login facebook', async ({ client }) => {
  const response = await client
  .put('/api/v1/users/facebook')
  .send({
    fb_token : fb_token
  }).end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status : 'Success'
  })
  token = response.body.data.token
  key  = response.body.data.key
})

test('Login facebook not success', async ({ client }) => {
  const response = await client
  .put('/api/v1/users/facebook')
  .send({
    fb_token : chance.word({ length: 50 })
  }).end()
  response.assertStatus(400)
  response.assertJSONSubset({
    status : 'Error',
    messages : 'Token not found'
  })
})

test('Login facebook ID is null', async ({ client }) => {
  const response = await client
  .put('/api/v1/users/facebook')
  .send({
    fb_token : ''
  }).end()
  response.assertStatus(400)
  response.assertJSONSubset({
    status : 'Error',
    messages : 'Token is null'
  })
})

test('Check token facebook', async ({ client }) => {
  const response = await client
  .put('/api/v1/users/token')
  .header('Authorization', 'Bearer '+token)
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status : 'Success'
  })
})

test('Check token facebook UID', async ({ client }) => {
  const response = await client
  .put('/api/v1/users/token')
  .header('Authorization', 'Bearer '+chance.word({ length: 50 }))
  .send({
    key : key
  })
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status : 'Success'
  })
  token = response.body.data.token
})

test('Check token facebook UID not token', async ({ client }) => {
  const response = await client
  .put('/api/v1/users/token')
  .send({
    key : key
  })
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status : 'Success'
  })
  token = response.body.data.token
})

test('Delete facebook token has expired', async ({ client }) => {
  const response = await client
  .delete('/api/v1/users/me')
  .header('Authorization', 'Bearer '+chance.word({ length: 50 }))
  .end()
  response.assertStatus(400)
  response.assertJSONSubset({
    status: 'Error',
    messages: 'Token has expired'
  })
})

test('Delete facebook', async ({ client }) => {
  const response = await client
  .delete('/api/v1/users/me')
  .header('Authorization', 'Bearer '+token)
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status : 'Success'
  })
})

test('Check token facebook UID not found', async ({ client }) => {
  const response = await client
  .put('/api/v1/users/token')
  .header('Authorization', 'Bearer '+chance.word({ length: 50 }))
  .send({
    key : key
  })
  .end()
  response.assertStatus(400)
  response.assertJSONSubset({
    status : 'Error',
    messages : 'User not found'
  })
})

test('Check token not found', async ({ client }) => {
  const response = await client
  .put('/api/v1/users/token')
  .header('Authorization', 'Bearer '+chance.word({ length: 50 }))
  .end()
  response.assertStatus(400)
  response.assertJSONSubset({
    status : 'Error',
    messages : 'User not found'
  })
})

test('Delete token has expired', async ({ client }) => {
  const response = await client
  .delete('/api/v1/users/me')
  .header('Authorization', 'Bearer '+chance.word({ length: 50 }))
  .end()
  response.assertStatus(400)
  response.assertJSONSubset({
    status : 'Error',
    messages : 'Token has expired'
  })
})

test('Login google', async ({ client }) => {

  const response = await client
  .put('/api/v1/users/google')
  .send({
    gg_token : gg_token
  }).end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status : 'Success'
  })
  token = response.body.data.token
  key  = response.body.data.key
})

test('Login google token not found', async ({ client }) => {
  const response = await client
  .put('/api/v1/users/google')
  .send({
    gg_token : chance.word({ length: 50 })
  }).end()
  response.assertStatus(400)
  response.assertJSONSubset({
    status : 'Error',
    messages : 'Token not found'
  })
})

test('Login google token is null', async ({ client }) => {
  const response = await client
  .put('/api/v1/users/google')
  .send({
    gg_token : ''
  }).end()
  response.assertStatus(400)
  response.assertJSONSubset({
    status : 'Error',
    messages : 'Token is null'
  })
})

test('Check token google', async ({ client }) => {
  const response = await client
  .put('/api/v1/users/token')
  .header('Authorization', 'Bearer '+token)
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status : 'Success'
  })
})

test('Check token google UID', async ({ client }) => {
  const response = await client
  .put('/api/v1/users/token')
  .header('Authorization', 'Bearer '+chance.word({ length: 50 }))
  .send({
    key : key
  })
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status : 'Success'
  })

  token = response.body.data.token
})

test('Check token google UID not token', async ({ client }) => {
  const response = await client
  .put('/api/v1/users/token')
  .send({
    key : key
  })
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status : 'Success'
  })
  token = response.body.data.token
})

test('Delete google token has expired', async ({ client }) => {
  const response = await client
  .delete('/api/v1/users/me')
  .header('Authorization', 'Bearer '+chance.word({ length: 50 }))
  .end()
  response.assertStatus(400)
  response.assertJSONSubset({
    status: 'Error',
    messages: 'Token has expired'
  })
})

test('Delete google', async ({ client }) => {
  const response = await client
  .delete('/api/v1/users/me')
  .header('Authorization', 'Bearer '+token)
  .end()
  response.assertStatus(200)
  response.assertJSONSubset({
    status : 'Success'
  })
})

test('Check token google UID not found', async ({ client }) => {
  const response = await client
  .put('/api/v1/users/token')
  .header('Authorization', 'Bearer '+chance.word({ length: 50 }))
  .send({
    key : key
  })
  .end()
  response.assertStatus(400)
  response.assertJSONSubset({
    status : 'Error',
    messages : 'User not found'
  })
})
