'use strict'

const Chat = use('App/Models/Chat')
const ChatSocket = use('App/Models/ChatSocket')
const Logger = use('Logger')
const Ws = use('Ws')
const chats = Ws.getChannel('chat:*')

const mongoose = require('mongoose')

class ChatController {
  constructor ({ socket, request, auth }) {
    this.socket = socket
    this.request = request
    this.auth = auth
  }

  onMessage (message) {
    const { broadcast, emitTo } = chats.topic(message.topic)
    var condition = [mongoose.Types.ObjectId(this.auth.user.id), mongoose.Types.ObjectId(message.uid)]
    condition.sort()
    const chat = new Chat()
    chat.uids = condition
    chat.detail = message.detail
    chat.owner = this.auth.user.id
    chat.save()
    this.socket.emit('message', message)
    ChatSocket.update({
      uids: condition
    },{
      $set: { topic: message.topic}
    } ,{
      upsert: true
    },function(err){
      if(err){
        Logger.error('ChatController onMessage Error is %s',err)
      }
    })
    this.socket.broadcast('message', message)
  }


  onClose () {
    // same as: socket.on('close')
  }

  onError () {
    // same as: socket.on('error')
  }


}

module.exports = ChatController
