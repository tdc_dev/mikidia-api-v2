'use strict'
const Comment = use('App/Models/Comment')
const mongoose = require('mongoose')
const Logger = use('Logger')
var moment = require('moment')

class CommentController {

  async read ({ request, response, auth, params }) {
    try {
      await auth.check()
      try {
        let results = await Comment.aggregate()
          .lookup({ from: 'profiles', localField: 'uid', foreignField: 'uid', as: 'profile' })
          .unwind("$profile")
        let baseUrl = request.secure()? 'https://' : 'http://'
        baseUrl += request.headers().host + '/'
        await results.forEach(function(item, index, array) {
          if(item.profile.picture){
            array[index].profile.picture = baseUrl+item.profile.picture
          }else{
            array[index].profile.picture = baseUrl+'uploads/default_profile.jpg'
          }
        })
        return response.status(200).json({ 'status' : 'Success', 'data' : results })
      } catch (err) {
        Logger.error('CommentController read Error is %s',err)
        return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
      }
    } catch (err) {
      return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
    }
  }

  async create ({ request, response, auth, params }) {
    try {
      await auth.check()
      try {
        const { aid, vote, detail } = request.all()
        if(!aid){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'Advertise ID is empty' })
        }
        if(!vote && !detail){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'Vote or Comment is empty' })
        }
        const user = await auth.getUser()
        let details = {}
        var ObjectId = mongoose.Types.ObjectId
        const mycomment = await Comment.findOne({ uid: mongoose.Types.ObjectId(user.id),aid: mongoose.Types.ObjectId(aid) })
        if(mycomment){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'Advertise ID is duplicate' })
        }
        const comment = new Comment()
        comment.uid = user.id
        comment.aid = aid
        comment.detail = detail
        comment.vote = vote
        await comment.save()
        const comment_list = await Comment.findOne({ _id: comment._id })
        return response.status(200).json({ 'status' : 'Success', 'data' : comment_list })
      } catch (err) {
        Logger.error('CommentController read Error is %s',err)
        return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
      }
    } catch (err) {
      return response.status(400).json({ 'status' : 'Error', 'messages' : 'Token has expired' })
    }
  }

  async update ({ request, response, auth, params }) {
    try {
      await auth.check()
      try {
        const { cid, vote, detail } = request.all()
        if(!cid){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'Comment ID is empty' })
        }
        if(!vote && !detail){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'Vote or Comment is empty' })
        }
        const user = await auth.getUser()

        await Comment.updateOne({
          _id: mongoose.Types.ObjectId(cid)
        },
        {
          $set: {
            detail: detail,
            vote: vote
          }
        },
        function(err, doc){
          if(err){
            Logger.error('CommentController create update Error is %s',err)
            return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
          }
        })
        const comment_list = await Comment.findOne({ _id: cid })
        return response.status(200).json({ 'status' : 'Success', 'data' : comment_list })
      } catch (err) {
        Logger.error('CommentController update Error is %s',err)
        return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
      }
    } catch (err) {
      return response.status(400).json({ 'status' : 'Error', 'messages' : 'Token has expired' })
    }
  }

  async delete ({ request, response, auth, params }) {
    try {
      await auth.check()
      try {
        const { cid } = request.all()
        if(!cid){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'Comment ID is empty' })
        }
        const comment = await Comment.deleteMany({ _id: mongoose.Types.ObjectId(cid) })
        if(!comment.n){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'Can not delete' })
        }
        return response.status(200).json({ 'status' : 'Success' })
      } catch (err) {
        Logger.error('CommentController delete Error is %s',err)
        return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
      }
    } catch (err) {
      return response.status(400).json({ 'status' : 'Error', 'messages' : 'Token has expired' })
    }
  }

  async deleteAll ({ request, response, auth, params }) {
    try {
      await auth.check()
      try {
        const { aid } = request.all()
        if(!aid){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'Advertise ID is empty' })
        }
        const comment = await Comment.deleteMany({ aid: mongoose.Types.ObjectId(aid) })
        if(!comment.n){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'Can not delete' })
        }
        return response.status(200).json({ 'status' : 'Success' })
      } catch (err) {
        Logger.error('CommentController delete Error is %s',err)
        return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
      }
    } catch (err) {
      return response.status(400).json({ 'status' : 'Error', 'messages' : 'Token has expired' })
    }
  }

}

module.exports = CommentController
