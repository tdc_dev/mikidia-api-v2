'use strict'

const ChatSocket = use('App/Models/ChatSocket')
const Chat = use('App/Models/Chat')
const Logger = use('Logger')
const Profile = use('App/Models/Profile')
const mongoose = require('mongoose')
const md5 = require('md5')
const Config = use('Config')
const appKey = Config.get('app.appKey')
const Helpers = use('Helpers')

class ChatController {
  async read ({ request, response, params, auth }) {
    try {
      await auth.check()
      try {
        const { uid } = params
        if(!uid){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'Not found user' })
        }
        const profile = await Profile.findOne({ uid : mongoose.Types.ObjectId(uid) })
        if(!profile){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'Profile found user' })
        }
        let baseUrl = request.secure()? 'https://' : 'http://'
        baseUrl += request.headers().host + '/'
        if(profile.picture){
          profile.picture = baseUrl+profile.picture
        }else{
          profile.picture =  baseUrl+'uploads/default_profile.jpg'
        }
        if(!profile.display_name){
          if(profile.email)
            profile.display_name = profile.email
          else
            profile.display_name = profile.phone
        }
        const user = await auth.getUser()
        const myprofile = await Profile.findOne({ uid : mongoose.Types.ObjectId(user.id) })
        if(myprofile.picture){
          myprofile.picture = baseUrl+myprofile.picture
        }else{
          myprofile.picture =  baseUrl+'uploads/default_profile.jpg'
        }
        if(!myprofile.display_name){
          if(myprofile.email)
            myprofile.display_name = myprofile.email
          else
            myprofile.display_name = myprofile.phone
        }
        const chatsocket = await ChatSocket.findOne({ uids: { $in: [uid, user.id] }})
        const chat = await Chat.find({ uids: { $in: [uid, user.id] }})

        return response.status(200).json({ 'status' : 'Success', 'data' : {
          chatsocket: chatsocket,
          profile: profile,
          myprofile: myprofile,
          chat: chat
        } })
      } catch (err) {
        Logger.error('ProfileController read getUser Error is %s',err)
        return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
      }
    } catch (err) {
      return response.status(400).json({ 'status' : 'Error', 'messages' : 'Token has expired' })
    }
  }
  async savefile ({ request, response, auth }) {
    try {
      await auth.check()
      try {
        const image = request.file('picture', {
          types: ['image'],
          size: '2mb'
        })
        const user = await auth.getUser()
        const uid = user.id
        const path = 'uploads/'+uid+'/chat/'
        const tmp = Helpers.publicPath('uploads')+'/'+uid+'/chat/'
        if(image){
          var file_name = `${new Date().getTime()}.${image.subtype}`
          await image.move(tmp, {
            name: file_name
          })
          if (!image.moved()) {
            Logger.error('ChatController savefile Error is %s',image.error().message  )
            return response.status(400).json({ 'status' : 'Error', 'messages' : image.error().message })
          }
          let baseUrl = request.secure()? 'https://' : 'http://'
          baseUrl += request.headers().host + '/'
          return response.status(200).json({ 'status' : 'Success', 'file_name' : baseUrl+path+file_name })
        }
        return response.status(400).json({ 'status' : 'Error', 'messages' : 'Cat not upload' })
      } catch (err) {
        Logger.error('ProfileController read getUser Error is %s',err)
        return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
      }
    } catch (err) {
      return response.status(400).json({ 'status' : 'Error', 'messages' : 'Token has expired' })
    }
  }
}

module.exports = ChatController
