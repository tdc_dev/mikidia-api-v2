'use strict'

const Profile = use('App/Models/Profile')
const Token = use('App/Models/Token')
const Logger = use('Logger')
const Helpers = use('Helpers')
const fs = use('fs')
const download = require('image-downloader')
const rmfr = require('rmfr')
const mongoose = require('mongoose')

class ProfileController {

  async read ({ request, response, auth, params }) {
    try {
      let { uid } = params
      if(!uid){
        return response.status(400).json({ 'status' : 'Error', 'messages' : 'User ID is empty' })
      }
      let result = await Profile.findOne({ uid : mongoose.Types.ObjectId(uid) })
      let baseUrl = request.secure()? 'https://' : 'http://'
      baseUrl += request.headers().host + '/'
      if(result.picture){
        result.picture = baseUrl+result.picture
      }else{
        result.picture =  baseUrl+'uploads/default_profile.jpg'
      }
      return response.status(200).json({ 'status' : 'Success', 'data' : result })
    } catch (err) {
      Logger.error('ProfileController read getUser Error is %s',err)
      return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
    }
  }

  async readMe ({ request, response, auth, params }) {
    try {
      await auth.check()
      try {
        const user = await auth.getUser()
        const uid = user.id
        if(!uid){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'User ID is empty' })
        }
        let result = await Profile.findOne({ uid : mongoose.Types.ObjectId(uid) })
        let baseUrl = request.secure()? 'https://' : 'http://'
        baseUrl += request.headers().host + '/'
        if(result.picture){
          result.picture = baseUrl+result.picture
        }else{
          result.picture =  baseUrl+'uploads/default_profile.jpg'
        }
        return response.status(200).json({ 'status' : 'Success', 'data' : result })
      } catch (err) {
        Logger.error('ProfileController read getUser Error is %s',err)
        return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
      }
    } catch (err) {
      return response.status(400).json({ 'status' : 'Error', 'messages' : 'Token has expired' })
    }
  }

  async write ({ request, response, auth, params }) {
    try {
      await auth.check()
      try {
        const { display_name, phone, email, birthday, location, picture } = request.all()
        const user = await auth.getUser()
        if(!user){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'Not found user' })
        }
        const image = request.file('picture', {
          types: ['image'],
          size: '2mb'
        })
        const removeFile = Helpers.promisify(fs.unlink)
        const uid = user.id
        const path = 'uploads/'+uid+'/me/'
        const tmp = Helpers.publicPath('uploads')+'/'+uid+'/me/'
        var files
        if(image){
          var file_name = `${new Date().getTime()}.${image.subtype}`
          await image.move(tmp, {
            name: file_name
          })
          if (!image.moved()) {
            Logger.error('ProfileController write picture Error is %s',image.error().message  )
            return response.status(400).json({ 'status' : 'Error', 'messages' : image.error().message })
          }
        }else if(picture){
          var file_name = `${new Date().getTime()}.`+'jpg'
          const options = {
            url: picture,
            dest: tmp+file_name
          }
          if(!fs.existsSync(Helpers.publicPath('uploads')+'/'+uid)){
            await fs.mkdirSync(Helpers.publicPath('uploads')+'/'+uid)
          }
          if(!fs.existsSync(Helpers.publicPath('uploads')+'/'+uid+'/me')){
            await fs.mkdirSync(Helpers.publicPath('uploads')+'/'+uid+'/me')
          }
          await download.image(options)
          /*
          .then(({ filename, image }) => {
            console.log(2)
          })
          */
          .catch((err) => {
            throw new Error( err.message )
          })
        }

        await fs.exists(tmp, (exists) => {
          if(exists){
            fs.readdirSync(tmp).forEach(function(element) {
              if(element != file_name)
                removeFile(tmp+element)
            })
          }
        })
        if(image || picture){
          var picture_name = path+file_name
        }
        var value = {
          display_name: display_name,
          phone: phone,
          email: email,
          birthday: birthday,
          location: location,
          picture: picture_name
        }

        if(!image && !picture) delete value.picture
        await Profile.update({ uid: mongoose.Types.ObjectId(user.id) },value,{
          upsert: true
        },function(err){
          if(err){
            Logger.error('ProfileController write update Error is %s',err)
            return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
          }
        })
        params = { uid: user.id }
        return this.read({ request, response, auth, params })
      } catch (err) {
        Logger.error('ProfileController write getUser Error is %s',err)
        return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
      }
    } catch (err) {
      return response.status(400).json({ 'status' : 'Error', 'messages' : 'Token has expired' })
    }
  }

  async delete ({ request, response, auth }) {
    try {
      await auth.check()
      try {
        const user = await auth.getUser()
        const uid = user.id
        const profile = await Profile.deleteMany({ uid: mongoose.Types.ObjectId(uid) })
        if(!profile.n){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'Can not delete' })
        }
        const tmp = Helpers.publicPath('uploads')+'/'+uid
        if(fs.existsSync(tmp))  await rmfr(tmp)
        return response.status(200).json({ 'status' : 'Success' })
      } catch (err) {
        Logger.error('ProfileController read getUser Error is %s',err)
        return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
      }
    } catch (err) {
      return response.status(400).json({ 'status' : 'Error', 'messages' : 'Token has expired' })
    }
  }

}

module.exports = ProfileController
