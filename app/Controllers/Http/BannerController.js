'use strict'

const Banner = use('App/Models/Banner')
const Helpers = use('Helpers')
const mongoose = require('mongoose')
const Logger = use('Logger')
const fs = use('fs')
const Config = use('Config')
const pageLimit = parseInt(Config.get('app.pageLimit'))

let today = new Date()
let mm = today.getMonth()+1
let yyyy = today.getFullYear()
let day_now = yyyy+'-'+mm

class BannerController {

  async create ({ request, response, auth }) {
    try {
      await auth.check()
      try {
        const { slot, position, start_date, end_date, link, status } = request.all()
        const image = request.file('picture', {
          types: ['image'],
          size: '2mb'
        })
        if(!image){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'Picture is empty' })
        }
        const path = 'uploads/banner/'+day_now+'/'
        const tmp = Helpers.publicPath('uploads')+'/banner/'+day_now+'/'
        var file_name = `${new Date().getTime()}.${image.subtype}`
        await image.move(tmp, {
          name: file_name
        })
        if (!image.moved()) {
          Logger.error('BannerController create picture Error is %s',image.error().message  )
          return response.status(400).json({ 'status' : 'Error', 'messages' : image.error().message })
        }
        const banner = new Banner()
        banner.picture = path+file_name
        banner.slot = slot
        banner.position = position
        banner.start_date = start_date
        banner.end_date = end_date
        banner.link = link
        banner.status = status
        await banner.save()
        let baseUrl = request.secure()? 'https://' : 'http://'
        baseUrl += request.headers().host + '/'
        if(banner.picture){
          banner.picture = baseUrl+banner.picture
        }
        return response.status(200).json({ 'status' : 'Success' , 'data' : banner })
      } catch (err) {
        Logger.error('BannerController create Error is %s',err)
        return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
      }
    } catch (err) {
      return response.status(400).json({ 'status' : 'Error', 'messages' : 'Token has expired' })
    }
  }

  async update ({ request, response, params, auth }) {
    try {
      await auth.check()
      try {
        const { bid, slot, position, start_date, end_date, link, status } = request.all()
        if(!bid){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'Banner ID is empty' })
        }
        const image = request.file('picture', {
          types: ['image'],
          size: '2mb'
        })
        if(!image){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'Picture is empty' })
        }
        const removeFile = Helpers.promisify(fs.unlink)
        let result = await Banner.findOne({ _id : mongoose.Types.ObjectId(bid) })

        const path = 'uploads/banner/'+day_now+'/'
        const tmp = Helpers.publicPath('uploads')+'/banner/'+day_now+'/'
        var file_name = `${new Date().getTime()}.${image.subtype}`
        await image.move(tmp, {
          name: file_name
        })
        if (!image.moved()) {
          Logger.error('BannerController update picture Error is %s',image.error().message  )
          return response.status(400).json({ 'status' : 'Error', 'messages' : image.error().message })
        }
        await Banner.update({ _id: mongoose.Types.ObjectId(bid) },{
          picture: path+file_name,
          slot: slot,
          position: position,
          start_date: start_date,
          end_date: end_date,
          link: link,
          status: status
        },{},function(err){
          if(err){
            Logger.error('ProfileController write update Error is %s',err)
            return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
          }
        })
        await fs.exists(Helpers.publicPath('/')+result.picture, (exists) => {
          if(exists){
            removeFile(Helpers.publicPath('/')+result.picture)
          }
        })
        return response.status(200).json({ 'status' : 'Success' })
      } catch (err) {
        Logger.error('BannerController update Error is %s',err)
        return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
      }
    } catch (err) {
      return response.status(400).json({ 'status' : 'Error', 'messages' : 'Token has expired' })
    }
  }

  async updateView ({ request, response, auth }) {
    try {
      const { bid } = request.all()
      if(!bid){
        return response.status(400).json({ 'status' : 'Error', 'messages' : 'Banner ID is empty' })
      }
      const banner = await Banner.update({ _id: mongoose.Types.ObjectId(bid) },{
        $inc: {
           view: 1
        }
      })
      if(!banner.nModified){
        return response.status(400).json({ 'status' : 'Error', 'messages' : 'Cat not update view' })
      }
      return response.status(200).json({ 'status' : 'Success' })
    } catch (err) {
      Logger.error('BannerController updateView Error is %s',err)
      return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
    }
  }

  async updateClick ({ request, response, auth }) {
    try {
      const { bid } = request.all()
      if(!bid){
        return response.status(400).json({ 'status' : 'Error', 'messages' : 'Banner ID is empty' })
      }
      const banner = await Banner.update({ _id: mongoose.Types.ObjectId(bid) },{
        $inc: {
           click: 1
        }
      })
      if(!banner.nModified){
        return response.status(400).json({ 'status' : 'Error', 'messages' : 'Cat not update click' })
      }
      return response.status(200).json({ 'status' : 'Success' })
    } catch (err) {
      Logger.error('BannerController updateClick Error is %s',err)
      return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
    }
  }

  async readPosition ({ request, response, params, auth }) {
    try {
      const { position } = params
      if(!position){
        return response.status(400).json({ 'status' : 'Error', 'messages' : 'Position is empty' })
      }
      let results = await Banner.find({
         position: position,
         status: "Active",
         start_date : { $lte : new Date()},
         end_date : { $gte : new Date()}
      })
      let baseUrl = request.secure()? 'https://' : 'http://'
      baseUrl += request.headers().host + '/'
      await results.forEach(function(item, index, array) {
        if(item.picture){
          array[index].picture = baseUrl+item.picture
        }
      })
      return response.status(200).json({ 'status' : 'Success' , 'data' : results })
    } catch (err) {
      Logger.error('BannerController readPosition Error is %s',err)
      return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
    }
  }

  async readAll ({ request, response, params, auth }) {
    try {
      await auth.check()
      try {
        var condition = {}
        var page = request.get().page
        var position = request.get().position
        if(!page || page == 0) page = 1
        var pageSkip = (parseInt(page)*pageLimit)-pageLimit
        if(!pageSkip) pageSkip = 0
        if(position){
          condition = { position: position }
        }
        let results = await Banner.find(condition)
          .skip(pageSkip)
          .limit(pageLimit)
        let baseUrl = request.secure()? 'https://' : 'http://'
        baseUrl += request.headers().host + '/'
        await results.forEach(function(item, index, array) {
          if(item.picture){
            array[index].picture = baseUrl+item.picture
          }
        })
        return response.status(200).json({ 'status' : 'Success' , 'data' : results })
      } catch (err) {
        Logger.error('BannerController readAll Error is %s',err)
        return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
      }
    } catch (err) {
      return response.status(400).json({ 'status' : 'Error', 'messages' : 'Token has expired' })
    }
  }

  async readOne ({ request, response, params, auth }) {
    try {
      await auth.check()
      try {
        const { bid } = params
        if(!bid){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'Banner ID is empty' })
        }
        let results = await Banner.find({
           _id: bid
        })
        let baseUrl = request.secure()? 'https://' : 'http://'
        baseUrl += request.headers().host + '/'
        await results.forEach(function(item, index, array) {
          if(item.picture){
            array[index].picture = baseUrl+item.picture
          }
        })
        return response.status(200).json({ 'status' : 'Success' , 'data' : results })
      } catch (err) {
        Logger.error('BannerController readOne Error is %s',err)
        return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
      }
    } catch (err) {
      return response.status(400).json({ 'status' : 'Error', 'messages' : 'Token has expired' })
    }
  }

  async delete ({ request, response, params, auth }) {
    try {
      await auth.check()
      try {
        const { bid } = request.all()
        let result = await Banner.findOne({ _id: mongoose.Types.ObjectId(bid) })

        const banner = await Banner.deleteOne({ _id: mongoose.Types.ObjectId(bid) })
        if(!banner.n){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'Can not delete' })
        }
        const removeFile = Helpers.promisify(fs.unlink)
        await fs.exists(Helpers.publicPath('/')+result.picture, (exists) => {
          if(exists){
            removeFile(Helpers.publicPath('/')+result.picture)
          }
        })
        return response.status(200).json({ 'status' : 'Success' })
      } catch (err) {
        Logger.error('BannerController delete Error is %s',err)
        return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
      }
    } catch (err) {
      return response.status(400).json({ 'status' : 'Error', 'messages' : 'Token has expired' })
    }
  }

}

module.exports = BannerController
