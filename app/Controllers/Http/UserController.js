'use strict'

const User = use('App/Models/User')
const Token = use('App/Models/Token')
const Logger = use('Logger')
const Profile = use('App/Models/Profile')
const Helpers = use('Helpers')
const fs = use('fs')
const jwt = require('jsonwebtoken')
const Client = require('node-rest-client').Client
const md5 = require('md5')
const Mail = use('Mail')
const Config = use('Config')
const appKey = Config.get('app.appKey')
const rmfr = require('rmfr')

var res

class UserController {


  async createUsername ({ request, response, auth }) {
    const { username, password } = request.all()
    try {
      if(!username){
        return response.status(400).json({ 'status' : 'Error', 'messages' : 'Username is null' })
      }
      if(await User.find({ username: username}).count() && username){
        return response.status(400).json({ 'status' : 'Error', 'messages' : 'Username duplicate' })
      }
      if(!password){
        return response.status(400).json({ 'status' : 'Error', 'messages' : 'Password is null' })
      }
      var phone = /^(0[0-9]{9})$/.test(username)
      var email = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(username)
      if (!phone && !email) {
        return response.status(400).json({ 'status' : 'Error', 'messages' : 'Username format invalid' })
      }
      const user = new User()
      user.username = username
      user.password = password
      await user.save()
      return this.loginUsername({ request, response, auth })
    } catch (err) {
      Logger.error('UserController createUsername Error is %s', err)
      return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
    }
  }

  async createFacebook (fbid, response) {
    try {
      if(!fbid){
        return response.status(400).json({ 'status' : 'Error', 'messages' : 'Facebook ID is null' })
      }
      const user = new User()
      user.fbid = fbid
      await user.save()
    } catch (err) {
      Logger.error('UserController createFacebook Error is %s', err)
      return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
    }
  }

  async createGoogle (gid, response) {
    try {
      if(!gid){
        return response.status(400).json({ 'status' : 'Error', 'messages' : 'Google ID is null' })
      }
      const user = new User()
      user.gid = gid
      await user.save()
    } catch (err) {
      Logger.error('UserController createGoogle Error is %s', err)
      return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
    }
  }

  async loginUsername ({ request, response, auth }) {
    try {
      const { username, password } = request.all()
      if(!username){
        return response.status(400).json({ 'status' : 'Error', 'messages' : 'Username is null' })
      }
      if(!password){
        return response.status(400).json({ 'status' : 'Error', 'messages' : 'Password is null' })
      }
      return this.generateToken(request.all(), response, auth)
    } catch (err) {
      Logger.error('UserController loginUsername Error is %s', err)
      return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
    }
  }

  async loginFacebook ({ request, response, auth, ally }) {
    try {
      const { fb_token } = request.all()
      if(!fb_token){
        return response.status(400).json({ 'status' : 'Error', 'messages' : 'Token is null' })
      }
      try {
        const facebook = await ally.driver('facebook').getUserByToken(fb_token)
        const fbid = facebook._userFields.id
        let user =  await User.findOne({ fbid: fbid })
        if(!user && fbid){
           await this.createFacebook(fbid, response)
           user =  await User.findOne({ fbid: fbid })
        }
        return this.token( md5(appKey+user.id), response ,auth )
      } catch (err) {
        return response.status(400).json({ 'status' : 'Error', 'messages' : 'Token not found' })
      }
    } catch (err) {
      Logger.error('UserController loginFacebook Error is %s', err)
      return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
    }
  }

  async loginGoogle ({ request, response, auth, ally }) {
    try {
      const { gg_token } = request.all()
      if(!gg_token){
        return response.status(400).json({ 'status' : 'Error', 'messages' : 'Token is null' })
      }
      try {
        const google = await ally.driver('google').getUserByToken(gg_token)
        const gid = google._userFields.id
        let user =  await User.findOne({ gid: gid })
        if(!user && gid){
          await this.createGoogle(gid, response)
          user =  await User.findOne({ gid: gid })
        }
        return this.token( md5(appKey+user.id), response ,auth )
      } catch (err) {
        return response.status(400).json({ 'status' : 'Error', 'messages' : 'Token not found' })
      }
    } catch (err) {
      Logger.error('UserController loginGoogle Error is %s', err)
      return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
    }
  }

  async generateToken (data, response, auth) {
    try {
      const { username, password } = data
      let user = await User.findOne({ username: username})
      if(!user){
        return response.status(400).json({ 'status' : 'Error', 'messages' : 'User not found' })
      }
      await Token.deleteMany({ uid : user.id })
      return response.status(200).json({'status' : 'Success', 'data' : {'token' : (await auth.attempt(username, password)).token, 'key' : md5(appKey+user.id), 'uid' : user.id }})
    } catch (err) {
      Logger.error('UserController generateToken Error is %s', err)
      return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
    }
  }

  async getToken ({ request, response ,auth }) {
    try {
      const { key } = request.all()
      return this.token( key, response ,auth )
    } catch (err) {
      Logger.error('UserController getToken Error is %s', err)
      return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
    }
  }

  async token ( key, response, auth ) {
    try {
      await auth.check()
      return response.status(200).json({ 'status' : 'Success', 'data' : await auth.getUser() })
    } catch (err) {
      try {
        const user = await User.findOne({ key: key })
        if(!user){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'User not found' })
        }
        await Token.deleteMany({ uid: user.id })
        var data = {'token' : (await auth.generate(user)).token, 'key' : md5(appKey+user.id), 'uid' : user.id }
        return response.status(200).json({ 'status' : 'Success', data })
      } catch (err) {
        Logger.error('UserController token Error is %s', err)
        return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
      }
    }
  }

  async delete ({ request, response ,auth }) {
    try {
      await auth.check()
      try {
        const user = await auth.getUser()
        if(!user){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'User not found' })
        }
        const uid = user.id
        const token = await Token.deleteMany({ uid: uid })
        if(!token.n){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'Can not delete' })
        }
        const usercheck = await User.deleteMany({ _id: uid })
        if(!usercheck.n){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'Can not delete' })
        }
        await Profile.deleteMany({ uid: uid })
        const tmp = Helpers.publicPath('uploads')+'/'+uid+'/'
        if(fs.existsSync(tmp)) await rmfr(tmp)
        return response.status(200).json({ 'status' : 'Success' })
      } catch (err) {
        Logger.error('UserController delete getUser Error is %s', err)
        return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
      }
    } catch (err) {
        return response.status(400).json({ 'status' : 'Error', 'messages' : 'Token has expired' })
    }
  }

  async emailForgotPassword ({ request, response }) {
    try {
      let baseUrl = request.secure()? 'https://' : 'http://'
      baseUrl += request.headers().host + '/'
      const { email } = request.all()
      if(!/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(email)){
        return response.status(400).json({ 'status' : 'Error', 'messages' : 'Email format invalid' })
      }
      const profile = await Profile.findOne({ email : email})
      if(!profile){
        return response.status(400).json({ 'status' : 'Error', 'messages' : 'Email not found' })
      }
      await Mail.send('emails/forgotpassword.edge', {
        username: profile.display_name,
        ulr: baseUrl+'forgotpassword/'+md5(appKey+profile.uid),
        facebook_url: '',
        twitter_url: '',
        instagram_url: '',
        email: email
      }, (message) => {
        message
          .to(email)
          .from('webmaster@hybridbold.com', 'Mikidia')
          .subject('กรุณายืนยันการ Reset Password ของ Mikidia.com')
      })
      return response.status(200).json({ 'status' : 'Success' })
    } catch (err) {
      Logger.error('UserController emailForgotPasswordConfirm Error is %s', err)
      return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
    }
  }

}

module.exports = UserController
