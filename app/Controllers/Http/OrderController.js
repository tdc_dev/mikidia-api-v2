'use strict'

const Order = use('App/Models/Order')
const OrderDetail = use('App/Models/OrderDetail')
const Logger = use('Logger')
const mongoose = require('mongoose')
const Helpers = use('Helpers')
const fs = use('fs')
const rmfr = require('rmfr')

class OrderController {

  async create({ request, response, auth }) {
    try {
      await auth.check()
      try {
        const { uid_saller, price, detail, location, view, expire } = request.all()
        if(!uid_saller){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'ID Saller is empty' })
        }
        if(!expire){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'Expire day is empty' })
        }
        const image = request.file('picture', {
          types: ['image'],
          size: '2mb'
        })
        const user = await auth.getUser()
        const order = new Order()
        order.uid_buyer =  mongoose.Types.ObjectId(user.id)
        order.uid_saller =  mongoose.Types.ObjectId(uid_saller)
        order.detail = detail
        order.price = price
        order.location = location
        order.view = view
        order.expire = expire
        await order.save()
        const oid = order._id
        if(image){
          const order_detail = new OrderDetail()
          order_detail.oid =  mongoose.Types.ObjectId(oid)
          await order_detail.save()
          const odid = order_detail._id
          const uid = user.id
          const path = 'uploads/'+uid+'/order/'+oid+'/'+odid+'/'
          const tmp = Helpers.publicPath('uploads')+'/'+uid+'/order/'+oid+'/'+odid+'/'
          var number = 0
          await image.moveAll(tmp, (file) => {
            number++
            return {
              name: `${new Date().getTime()}-${number}.${file.subtype}`
            }
          })
          var picture = []
          await image._files.forEach(function(item, index, array) {
            picture.push(path+item.fileName)
          })

          await OrderDetail.updateOne({ _id: odid }, { $set: { picture: picture }} )
        }
        let params = { oid : oid }
        return await this.read({ request, response, params, auth })
      } catch (err) {
        Logger.error('OrderController create Error is %s',err)
        return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
      }
    } catch (err) {
      return response.status(400).json({ 'status' : 'Error', 'messages' : 'Token has expired' })
    }
  }

  async update({ request, response, auth }){
    try {
      await auth.check()
      try {
        const { oid, odid, uid_saller, price, detail, location, view, expire, status, remark } = request.all()
        if(!oid){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'Order ID is empty' })
        }
        if(!uid_saller){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'ID Saller is empty' })
        }
        const user = await auth.getUser()
        const image = request.file('picture', {
          types: ['image'],
          size: '2mb'
        })
        var order = await Order.update({
          _id: mongoose.Types.ObjectId(oid)
        }, { $set: {
          detail: detail,
          price: price,
          location: location,
          view: view,
          expire: expire,
          status: status
        }})
        if(!order.nModified){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'Cat not update' })
        }
        if(image){
          if(!odid){
            const order_detail = new OrderDetail()
            order_detail.oid =  mongoose.Types.ObjectId(oid)
            await order_detail.save()
            var order_detail_id = order_detail._id
          }else{
            var order_detail_id = odid
          }
          const uid = user.id
          const path = 'uploads/'+uid+'/order/'+oid+'/'+order_detail_id+'/'
          const tmp = Helpers.publicPath('uploads')+'/'+uid+'/order/'+oid+'/'+order_detail_id+'/'
          var number = 0
          await image.moveAll(tmp, (file) => {
            number++
            return {
              name: `${new Date().getTime()}-${number}.${file.subtype}`
            }
          })
          var files = [],
          picture = []
          await image._files.forEach(function(item, index, array) {
            files.push(item.fileName)
            picture.push(path+item.fileName)
          })
          const removeFile = Helpers.promisify(fs.unlink)
          await fs.exists(tmp, (exists) => {
            if(exists){
              fs.readdirSync(tmp).forEach(function(element) {
                if(files.indexOf(element) < 0)
                  removeFile(tmp+element)
              })
            }
          })
          await OrderDetail.updateOne({ _id: order_detail_id }, { $set: { picture: picture, remark: remark }} )
        }
        let params = { oid : oid }
        return await this.read({ request, response, params, auth })
      } catch (err) {
        Logger.error('OrderController update Error is %s',err)
        return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
      }
    } catch (err) {
      return response.status(400).json({ 'status' : 'Error', 'messages' : 'Token has expired' })
    }
  }

  async read({ request, response, params, auth }){
    try {
      await auth.check()
      try {
        const { oid } = params
        if(!oid){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'Order ID is empty' })
        }
        let results = await Order.aggregate()
          .match({ _id: mongoose.Types.ObjectId(oid) })
          .lookup({ from: 'order_details', localField: '_id', foreignField: 'oid', as: 'order_details' })
        if(!results){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'Order ID not found' })
        }
        let baseUrl = request.secure()? 'https://' : 'http://'
        baseUrl += request.headers().host + '/'
        var result = {}
        await results.forEach(function(item, index, array) {
          item.order_details.forEach(function(item_details, index_details, array_details) {
            item_details.picture.forEach(function(item_picture, index_picture, array_picture) {
              if(item_picture){
                array_picture[index_picture] = baseUrl+item_picture
              }
            })
          })
          result = item
        })
        return response.status(200).json({ 'status' : 'Success', 'data' : result })
      } catch (err) {
        Logger.error('OrderController read Error is %s',err)
        return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
      }
    } catch (err) {
      return response.status(400).json({ 'status' : 'Error', 'messages' : 'Token has expired' })
    }
  }

  async readUser({ request, response, params, auth }){
    try {
      await auth.check()
      try {
        const user = await auth.getUser()
        if(!user.id){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'Order ID is empty' })
        }
        let results = await Order.aggregate()
          .match(
            {
                $or:[
                  { uid_buyer: mongoose.Types.ObjectId(user.id) },
                  { uid_saller: mongoose.Types.ObjectId(user.id) }
                ]
            }
          )
          .lookup({ from: 'order_details', localField: '_id', foreignField: 'oid', as: 'order_details' })
          .lookup({ from: 'profiles', localField: 'uid_buyer', foreignField: 'uid', as: 'profiles_buyer' })
          .lookup({ from: 'profiles', localField: 'uid_saller', foreignField: 'uid', as: 'profiles_saller' })
        if(!results){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'Order ID not found' })
        }
        let baseUrl = request.secure()? 'https://' : 'http://'
        baseUrl += request.headers().host + '/'
        var result = {}
        await results.forEach(function(item, index, array) {
          array[index].contact_profiles = []
          item.order_details.forEach(function(item_details, index_details, array_details) {
            item_details.picture.forEach(function(item_picture, index_picture, array_picture) {
              if(item_picture){
                array_picture[index_picture] = baseUrl+item_picture
              }
            })
          })
          item.profiles_buyer.forEach(function(item_profiles_buyer, index_profiles_buyer, array_profiles_buyer) {
            if(item_profiles_buyer.picture){
              array_profiles_buyer[index_profiles_buyer].picture = baseUrl+item_profiles_buyer.picture
            }else{
              array_profiles_buyer[index_profiles_buyer].picture =  baseUrl+'uploads/default_profile.jpg'
            }
            if(item.profiles_buyer == user.id){
              array[index].contact_profiles = item_profiles_buyer
            }
          })
          item.profiles_saller.forEach(function(item_profiles_saller, index_profiles_saller, array_profiles_saller) {
            if(item_profiles_saller.picture){
              array_profiles_saller[index_profiles_saller].picture = baseUrl+item_profiles_saller.picture
            }else{
              array_profiles_saller[index_profiles_saller].picture =  baseUrl+'uploads/default_profile.jpg'
            }
            if(item.uid_saller == user.id){
              array[index].contact_profiles = item_profiles_saller
            }
          })
          if(item.uid_buyer == user.id){
            array[index].order_status = 'buyer'
          }else{
            array[index].order_status = 'saller'
          }
          delete item.profiles_buyer
          delete item.profiles_saller
        })
        return response.status(200).json({ 'status' : 'Success', 'data' : results })
      } catch (err) {
        Logger.error('OrderController read Error is %s',err)
        return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
      }
    } catch (err) {
      return response.status(400).json({ 'status' : 'Error', 'messages' : 'Token has expired' })
    }
  }

  async delete({ request, response, auth }){
    try {
      await auth.check()
      try {
        const { oid } = request.all()
        if(!oid){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'Order ID is empty' })
        }
        const user = await auth.getUser()
        const order = await Order.deleteMany({ _id: mongoose.Types.ObjectId(oid) })
        if(!order.n){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'Can not delete order' })
        }
        const order_detail = await OrderDetail.deleteMany({ oid: mongoose.Types.ObjectId(oid) })
        if(!order_detail.n){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'Can not delete order detail' })
        }
        const uid = user.id
        const tmp = Helpers.publicPath('uploads')+'/'+uid+'/order/'+oid+'/'
        if(fs.existsSync(tmp))  await rmfr(tmp)
        return response.status(200).json({ 'status' : 'Success' })
      } catch (err) {
        Logger.error('OrderController delete Error is %s',err)
        return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
      }
    } catch (err) {
      return response.status(400).json({ 'status' : 'Error', 'messages' : 'Token has expired' })
    }
  }

}

module.exports = OrderController
