'use strict'

const Advertise = use('App/Models/Advertise')
const Profile = use('App/Models/Profile')
const Logger = use('Logger')
const Config = use('Config')
const Helpers = use('Helpers')
const fs = use('fs')
const pageLimit = parseInt(Config.get('app.pageLimit'))
let where = { id : 1 }
const rmfr = require('rmfr')
const mongoose = require('mongoose')

class AdvertiseController {

  async create ({ request, response, params, auth }) {
    try {
      await auth.check()
      try {
        const { title, detail, price, location, view } = request.all()
        if(!title){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'Title is empty' })
        }
        const image = request.file('picture', {
          types: ['image'],
          size: '2mb'
        })
        if(!image){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'Picture is empty' })
        }
        const user = await auth.getUser()
        const advertise = new Advertise()
        advertise.uid = mongoose.Types.ObjectId(user.id)
        advertise.title = title
        advertise.detail = detail
        advertise.price = price
        advertise.location = location
        advertise.view = view
        await advertise.save()
        const aid = advertise._id
        const uid = user.id
        const path = 'uploads/'+uid+'/advertise/'+aid+'/'
        const tmp = Helpers.publicPath('uploads')+'/'+uid+'/advertise/'+aid+'/'
        var number = 0
        await image.moveAll(tmp, (file) => {
          number++
          return {
            name: `${new Date().getTime()}-${number}.${file.subtype}`
          }
        })
        var files = [],
        picture = []
        await image._files.forEach(function(item, index, array) {
          files.push(item.fileName)
          picture.push(path+item.fileName)
        })
        await Advertise.updateOne({ _id: aid }, { $set: { picture: picture }} )
        let params = { aid : aid }
        return await this.readOne({ request, response, params, auth })
      } catch (err) {
        Logger.error('AdvertiseController create Error is %s',err)
        return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
      }
    } catch (err) {
      return response.status(400).json({ 'status' : 'Error', 'messages' : 'Token has expired' })
    }
  }

  async update ({ request, response, params, auth }) {
    try {
      await auth.check()
      try {
        const { aid, title, detail, price, location, view } = request.all()
        if(!aid){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'Advertise ID is empty' })
        }
        if(!title){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'Title is empty' })
        }
        const image = request.file('picture', {
          types: ['image'],
          size: '2mb'
        })
        if(!image){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'Picture is empty' })
        }
        const user = await auth.getUser()
        const uid = user.id
        const path = 'uploads/'+uid+'/advertise/'+aid+'/'
        const tmp = Helpers.publicPath('uploads')+'/'+uid+'/advertise/'+aid+'/'

        var number = 0
        await image.moveAll(tmp, (file) => {
          number++
          return {
            name: `${new Date().getTime()}-${number}.${file.subtype}`
          }
        })
        var files = [],
        picture = []
        await image._files.forEach(function(item, index, array) {
          files.push(item.fileName)
          picture.push(path+item.fileName)
        })
        const removeFile = Helpers.promisify(fs.unlink)
        await fs.exists(tmp, (exists) => {
          if(exists){
            fs.readdirSync(tmp).forEach(function(element) {
              if(files.indexOf(element) < 0)
                removeFile(tmp+element)
            })
          }
        })
        var advertise = await Advertise.update({
          _id: mongoose.Types.ObjectId(aid),
          uid: mongoose.Types.ObjectId(user.id)
        }, { $set: {
          title: title,
          detail: detail,
          price: price,
          location: location,
          view: view,
          picture: picture
        }})
        if(!advertise.nModified){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'Cat not update' })
        }
        let params = { aid :  mongoose.Types.ObjectId(aid) }
        return await this.readOne({ request, response, params, auth })
      } catch (err) {
        Logger.error('AdvertiseController update Error is %s',err)
        return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
      }
    } catch (err) {
      return response.status(400).json({ 'status' : 'Error', 'messages' : 'Token has expired' })
    }
  }

  async readOne ({ request, response, params, auth }) {
    try {
      const { aid } = params
      if(!aid){
        return response.status(400).json({ 'status' : 'Error', 'messages' : 'Advertise ID is empty' })
      }
      let results = await Advertise.aggregate()
        .match({ _id: mongoose.Types.ObjectId(aid) })
        .lookup({ from: 'profiles', localField: 'uid', foreignField: 'uid', as: 'profile' })
        .unwind('$profile')
      if(!results){
        return response.status(400).json({ 'status' : 'Error', 'messages' : 'Order ID not found' })
      }
      let baseUrl = request.secure()? 'https://' : 'http://'
      baseUrl += request.headers().host + '/'
      var result = {}
      await results.forEach(function(item, index, array) {
        if(item.profile.picture){
          array[index].profile.picture = baseUrl+item.profile.picture
        }else{
          array[index].profile.picture = baseUrl+'uploads/default_profile.jpg'
        }
        item.picture.forEach(function(itemPic, indexPic, arrayPic) {
          if(itemPic){
            arrayPic[indexPic] = baseUrl+itemPic
          }else{
            arrayPic[indexPic] = baseUrl+'uploads/default_profile.jpg'
          }
        })
        result = item
      })
      return response.status(200).json({ 'status' : 'Success', 'data' : result })

    } catch (err) {
      Logger.error('AdvertiseController readOne Error is %s',err)
      return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
    }
  }

  async readMe ({ request, response, auth }) {
    try {
      await auth.check()
      const user = await auth.getUser()
      where = { uid: mongoose.Types.ObjectId(user.id) }
      return this.read({ request, response, auth }, where)
    } catch (err) {
      return response.status(400).json({ 'status' : 'Error', 'messages' : 'Token has expired' })
    }
  }

  async readAll ({ request, response, auth }) {
    delete where.id
    return this.read({ request, response, auth }, where)
  }

  async read ({ request, response, auth }, where) {
    try {
      var sort = { id : 1 }
      const { title, detail, price, location, view, date, vote } = request.get()
      var page = request.get().page
      if(!page || page == 0) page = 1
      if(!(/^\d+$/.test(page))){
        return response.status(400).json({ 'status' : 'Error', 'messages' : 'Page format invalid' })
      }
      if(title) where.title = new RegExp(title, 'i')
      if(detail) where.detail = new RegExp(detail, 'i')
      if(price){
        var txtprice = price.split(',')
        if(!(/^\d+$/.test(txtprice[0]))){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'Price format invalid' })
        }
        if(!(/^\d+$/.test(txtprice[1]))){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'Price format invalid' })
        }
        where.price = { $gte: parseInt(txtprice[0]), $lte: parseInt(txtprice[1]) }
      }
      if(location) where.location = parseInt(location)
      if(view){
        var txtview = view.split(',')
        if(!(/^\d+$/.test(txtview[0]))){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'Price format invalid' })
        }
        if(!(/^\d+$/.test(txtview[1]))){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'Price format invalid' })
        }
        where.view = { $gte: parseInt(txtview[0]), $lte: parseInt(txtview[1]) }
      }
      if(date) sort.date = parseInt(date)
      if(vote) sort.vote = parseInt(vote)
      if(!title && !detail && !price && !location && !view) where = {}
      var pageSkip = (parseInt(page)*pageLimit)-pageLimit
      if(!pageSkip) pageSkip = 0
      let results = await Advertise.aggregate()
        .lookup({ from: 'profiles', localField: 'uid', foreignField: 'uid', as: 'profile' })
        .lookup({ from: 'comments', localField: '_id', foreignField: 'aid', as: 'comment' })
        .match(where)
        .project({
          "_id": "$_id",
          "picture": "$picture",
          "status": "$status",
          "uid": "$uid",
          "title": "$title",
          "detail": "$detail",
          "price": "$price",
          "location": "$location",
          "date": "$created_at",
          "view": "$view",
          "count": { $sum: 1 },
          "vote": {$sum: "$comment.vote"} ,
          "profile":  "$profile"
        })
        .skip(pageSkip)
        .limit(pageLimit)
        .sort(sort)
      let baseUrl = request.secure()? 'https://' : 'http://'
      baseUrl += request.headers().host + '/'
      await results.forEach(function(item, index, array) {
        item.profile.forEach(function(itemProfile, indexProfile, arrayProfile) {
          if(itemProfile.picture){
            arrayProfile[indexProfile].picture = baseUrl+itemProfile.picture
          }else{
            arrayProfile[indexProfile].picture = baseUrl+'uploads/default_profile.jpg'
          }
          item.profile = itemProfile
        })
        item.picture.forEach(function(itemPic, indexPic, arrayPic) {
          if(itemPic){
            arrayPic[indexPic] = baseUrl+itemPic
          }else{
            arrayPic[indexPic] = baseUrl+'uploads/default_profile.jpg'
          }
        })
      })
      return response.status(200).json({ 'status' : 'Success', 'data' : results })
    } catch (err) {
      Logger.error('AdvertiseController read Error is %s',err)
      return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
    }
  }

  async delete ({ request, response, params, auth }) {
    try {
      await auth.check()
      try {
        const { aid } = request.all()
        const user = await auth.getUser()
        const advertise = await Advertise.deleteMany({ _id: mongoose.Types.ObjectId(aid) })
        if(!advertise.n){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'Can not delete' })
        }
        const uid = user.id
        const tmp = Helpers.publicPath('uploads')+'/'+uid+'/advertise/'
        if(fs.existsSync(tmp))  await rmfr(tmp)
        return response.status(200).json({ 'status' : 'Success' })
      } catch (err) {
        Logger.error('AdvertiseController delete Error is %s',err)
        return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
      }
    } catch (err) {
      return response.status(400).json({ 'status' : 'Error', 'messages' : 'Token has expired' })
    }
  }

}

module.exports = AdvertiseController
