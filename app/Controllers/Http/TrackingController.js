'use strict'

const Tracking = use('App/Models/Tracking')
const Logger = use('Logger')
const mongoose = require('mongoose')

class TrackingController {

  async create({ request, response, auth }) {
    try {
      await auth.check()
      try {
        const { uid } = request.all()
        if(!uid){
          return response.status(400).json({ 'status' : 'Error', 'messages' : 'User ID is empty' })
        }
        let params
        const user = await auth.getUser()
        const uid_destination = uid
        const uid_source = user.id
        const tracking = await Tracking.findOne({
          uid_destination: mongoose.Types.ObjectId(uid_destination),
          uid_source: mongoose.Types.ObjectId(uid_source)
        })
        if(!tracking){
          const tracking = new Tracking()
          tracking.uid_destination =  mongoose.Types.ObjectId(uid_destination)
          tracking.uid_source =  mongoose.Types.ObjectId(uid_source)
          await tracking.save()
        }
        return response.status(200).json({ 'status' : 'Success' })
      } catch (err) {
        Logger.error('TrackingController create Error is %s',err)
        return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
      }
    } catch (err) {
      return response.status(400).json({ 'status' : 'Error', 'messages' : 'Token has expired' })
    }
  }

  async delete({ request, response, auth }){
    try {
      await auth.check()
      try {
        const { uid } = request.all()
        const user = await auth.getUser()
        const uid_destination = uid
        const uid_source = user.id
        const tracking = await Tracking.findOne({
          uid_destination: mongoose.Types.ObjectId(uid_destination),
          uid_source: mongoose.Types.ObjectId(uid_source)
        })
        if(tracking){
          await Tracking.findOneAndRemove({
             uid_destination: mongoose.Types.ObjectId(uid_destination),
             uid_source: mongoose.Types.ObjectId(uid_source)
           })
         }
        return response.status(200).json({ 'status' : 'Success' })
      } catch (err) {
        Logger.error('TrackingController delete Error is %s',err)
        return response.status(400).json({ 'status' : 'Error', 'messages' : err.message })
      }
    } catch (err) {
      return response.status(400).json({ 'status' : 'Error', 'messages' : 'Token has expired' })
    }
  }

}

module.exports = TrackingController
