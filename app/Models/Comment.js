'use strict'

const BaseModel = use('MongooseModel')
const mongoose = require('mongoose')

/**
 * @class Comment
 */
class Comment extends BaseModel {
  static boot ({ schema }) {
    // Hooks:
    // this.addHook('preSave', () => {})
    // this.addHook('preSave', 'CommentHook.method')
    // Indexes:
    // this.index({}, {background: true})
    //this.addHook('preFindOneAndUpdate', 'CommentHook.setProfile')
  }
  /**
   * Comment's schema
   */
  static get schema () {
    return {
      aid: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
      },
      uid: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
      },
      detail: {
        type: String
      },
      vote: {
        type: Number,
        default: 0
      }
    }
  }
}

module.exports = Comment.buildModel('Comment')
