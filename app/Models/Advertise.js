'use strict'

const BaseModel = use('MongooseModel')
const mongoose = require('mongoose')

/**
 * @class Advertise
 */
class Advertise extends BaseModel {
  static boot ({ schema }) {
    // Hooks:
    // this.addHook('preSave', () => {})
    // this.addHook('preSave', 'AdvertiseHook.method')
    // Indexes:
    // this.index({}, {background: true})
  }
  /**
   * Advertise's schema
   */
  static get schema () {
    return {
      uid: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
      },
      title: {
        type: String
      },
      detail: {
        type: String
      },
      price: {
        type: Number
      },
      location: {
        type: Number
      },
      view: {
        type: Number
      },
      picture: {
        type: [String]
      },
      status: {
        type: String,
        default: 'Unapprove'
      },
      created_at: {
        type: Date,
        select: false
      },
      updated_at: {
        type: Date,
        select: false
      },
      __v: {
        type: Number,
        select: false
      }
    }
  }
}

module.exports = Advertise.buildModel('Advertise')
