'use strict'

const BaseModel = use('MongooseModel')
const mongoose = require('mongoose')

/**
 * @class OrderDetail
 */
class OrderDetail extends BaseModel {
  static boot ({ schema }) {
    // Hooks:
    // this.addHook('preSave', () => {})
    // this.addHook('preSave', 'OrderDetailHook.method')
    // Indexes:
    // this.index({}, {background: true})
  }
  /**
   * OrderDetail's schema
   */
  static get schema () {
    return {
      oid: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
      },
      picture: {
        type: [String],
        required: true
      },
      remark: {
        type: String
      },
    }
  }
}

module.exports = OrderDetail.buildModel('Order_Detail')
