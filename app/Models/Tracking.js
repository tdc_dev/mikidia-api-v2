'use strict'

const BaseModel = use('MongooseModel')
const mongoose = require('mongoose')

/**
 * @class Tracking
 */
class Tracking extends BaseModel {
  static boot ({ schema }) {
    this.addHook('preSave', 'TrackingHook.addUserTrack')
    this.addHook('afterFindOneAndRemove', 'TrackingHook.subUserTrack')
  }
  /**
   * Tracking's schema
   */
  static get schema () {
    return {
      uid_source: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
      },
      uid_destination: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
      }
    }
  }
}

module.exports = Tracking.buildModel('Tracking')
