'use strict'

const UserHook = exports = module.exports = {}
const Hash = use('Hash')
const Profile = use('App/Models/Profile')
const md5 = require('md5');
const Config = use('Config')
const appKey = Config.get('app.appKey')

UserHook.hashPassword = async (user) => {
  if(user.password)
    user.password = await Hash.make(user.password)
}

UserHook.checkUsername = async (user) => {
  if(user.username){
    var phone = /^(0[0-9]{9})$/.test(user.username)
    var email = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(user.username)
    if (!phone && !email) {
      console.log(user.username)
      throw new Error('Username format invalid')
    }
  }
}

UserHook.insertUsername = async (user) => {
  if(user.username){
    try {
      const profile = new Profile()
      profile.uid = user.id
      var phone = /^(0[0-9]{9})$/.test(user.username)
      var email = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(user.username)
      if (phone) profile.phone = user.username
      else if (email) profile.email = user.username
      await profile.save()
    } catch (err) {
      throw new Error( err.message )
    }
  }
}

UserHook.setToken = async (user) => {
  try {
    const User = use('App/Models/User')
    var key = await User.updateOne({ _id: user.id }, { $set: { key: md5(appKey+user.id) } })
    if(!key.nModified){
      throw new Error( 'Token no generate' )
    }
  } catch (err) {
    throw new Error( err.message )
  }
}
