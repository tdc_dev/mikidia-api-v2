'use strict'

const Profile = use('App/Models/Profile')
const mongoose = require('mongoose')

const TrackingHook = exports = module.exports = {}

TrackingHook.addUserTrack = async (tracking) => {
  await Profile.update({ uid: mongoose.Types.ObjectId(tracking.uid_source) },{
    $inc: {
       following: 1
    }
  })
  await Profile.update({ uid: mongoose.Types.ObjectId(tracking.uid_destination) },{
    $inc: {
       follower: 1
    }
  })
}
TrackingHook.subUserTrack = async (tracking) => {
  await Profile.update({ uid: mongoose.Types.ObjectId(tracking._conditions.uid_source) },{
    $inc: {
       following: -1
    }
  })
  await Profile.update({ uid: mongoose.Types.ObjectId(tracking._conditions.uid_destination) },{
    $inc: {
       follower: -1
    }
  })
}
