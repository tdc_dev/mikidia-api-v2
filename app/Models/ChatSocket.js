'use strict'

const BaseModel = use('MongooseModel')
const mongoose = require('mongoose')

/**
 * @class ChatSocket
 */
class ChatSocket extends BaseModel {
  static boot ({ schema }) {
    // Hooks:
    // this.addHook('preSave', () => {})
    // this.addHook('preSave', 'ChatSocketHook.method')
    // Indexes:
    // this.index({}, {background: true})
  }
  /**
   * ChatSocket's schema
   */
  static get schema () {
    return {
      uids: {
        type: [String],
        required: true
      },
      topic: {
        type: String,
        required: true
      }
    }
  }
}

module.exports = ChatSocket.buildModel('Chat_Socket')
