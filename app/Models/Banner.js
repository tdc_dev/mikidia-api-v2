'use strict'

const BaseModel = use('MongooseModel')

/**
 * @class Banner
 */
class Banner extends BaseModel {
  static boot ({ schema }) {
    // Hooks:
    // this.addHook('preSave', () => {})
    // this.addHook('preSave', 'BannerHook.method')
    // Indexes:
    // this.index({}, {background: true})
  }
  /**
   * Banner's schema
   */
  static get schema () {
    return {
      picture: {
        type: String,
        required: true
      },
      slot: {
        type: Number,
        default: 0
      },
      position: {
        type: String
      },
      start_date: {
        type: Date
      },
      end_date: {
        type: Date
      },
      click: {
        type: Number
      },
      view: {
        type: Number
      },
      link: {
        type: String
      },
      status: {
        type: String,
        default: 'Deavtive'
      }

    }
  }
}

module.exports = Banner.buildModel('Banner')
