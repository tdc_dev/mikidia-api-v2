'use strict'

const BaseModel = use('MongooseModel')
const mongoose = require('mongoose')

/**
 * @class Profile
 */
class Profile extends BaseModel {

  static boot ({ schema }) {
    //findOne
  }

  /**
   * Profile's schema
   */
  static get schema () {
    return {
      __v: {
        type: Number,
        select: false
      },
      uid: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
      },
      display_name: {
        type: String
      },
      phone: {
        type: String,
        validate: {
          validator: function(v) {
            return /\d{10}/.test(v);
          },
          message: '{VALUE} is not a valid phone number!'
        }
      },
      email: {
        type: String,
        validate: {
          validator: function(v) {
            return /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(v);
          },
          message: '{VALUE} is not a valid email!'
        }
      },
      birthday: {
        type: Date
      },
      picture: {
        type: String
      },
      location: {
        type: Number
      },
      created_at: {
        type: Date,
        select: false
      },
      updated_at: {
        type: Date,
        select: false
      },
      follower: {
        type: Number,
        default: 0
      },
      following: {
        type: Number,
        default: 0
      }
    }
  }

}

module.exports = Profile.buildModel('Profile')
