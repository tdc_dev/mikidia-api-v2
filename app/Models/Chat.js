'use strict'

const BaseModel = use('MongooseModel')
const mongoose = require('mongoose')

/**
 * @class Chat
 */
class Chat extends BaseModel {
  static boot ({ schema }) {
    // Hooks:
    // this.addHook('preSave', () => {})
    // this.addHook('preSave', 'ChatHook.method')
    // Indexes:
    // this.index({}, {background: true})
  }
  /**
   * Chat's schema
   */
  static get schema () {
    return {
      uids: {
        type: [String],
        required: true
      },
      detail: {
        type: String
      },
      status: {
        type: String,
        default: 'unread'
      },
      owner: {
        type: String,
        required: true
      }
    }
  }
}

module.exports = Chat.buildModel('Chat')
