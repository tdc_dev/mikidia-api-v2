'use strict'

const BaseModel = use('MongooseModel')

/**
 * @class User
 */
class User extends BaseModel {
  static get primaryKeyValue () {
    return '_id'
  }
  static boot ({ schema }) {
    this.addHook('preSave', 'UserHook.checkUsername')
    this.addHook('preSave', 'UserHook.hashPassword')
    this.addHook('afterSave', 'UserHook.insertUsername')
    this.addHook('afterSave', 'UserHook.setToken')
  }
  /**
   * User's schema
   */
  static get schema () {
    return {
      fbid: {
        type: String
      },
      gid: {
        type: String
      },
      key: {
        type: String
      },
      username: {
        type: String
      },
      password: {
        type: String
      },
      status: {
        type: String,
        default: 'Active'
      }
    }
  }
  static get timestamps () {
    return true
  }
  static get incrementing () {
    return false
  }
}

module.exports = User.buildModel('User')
