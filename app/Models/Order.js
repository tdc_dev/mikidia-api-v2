'use strict'

const BaseModel = use('MongooseModel')
const mongoose = require('mongoose')

/**
 * @class order
 */
class Order extends BaseModel {
  static boot ({ schema }) {
    // Hooks:
    // this.addHook('preSave', () => {})
    // this.addHook('preSave', 'orderHook.method')
    // Indexes:
    // this.index({}, {background: true})
  }
  /**
   * order's schema
   */
  static get schema () {
    return {
      uid_buyer: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
      },
      uid_saller: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
      },
      price: {
        type: Number,
        required: true
      },
      status: {
        type: String,
        default: 'wait'
      },
      detail: {
        type: String
      },
      location: {
        type: Number
      },
      view: {
        type: Number
      },
      expire: {
        type: Date,
        required: true
      }
    }
  }
}

module.exports = Order.buildModel('Order')
