let ws = null
let url = new URL(document.URL)
let token = url.searchParams.get("token")
let uid = url.searchParams.get("uid")
let topic, myprofile, profile, myuid, picture, mypicture
$(function () {
  $.ajax({
    url: "/api/v1/chat/"+uid,
    type: 'GET',
    headers: {"Authorization": "Bearer "+token}
  }).done(function(res) {
    if(res.data.chatsocket){
      topic = res.data.chatsocket.topic
    }else{
      topic = "chat:mikidia:"+uid
    }
    display_name = res.data.myprofile.display_name
    myuid = res.data.myprofile.uid
    mypicture = res.data.myprofile.picture
    picture = res.data.profile.picture
    var html = ''
    res.data.chat.forEach(function(item, index, array) {
      if(myuid == item.owner){
        html +=`<div class="message right"><img class="box" src="${mypicture}" /><div class="box"><h3> ${display_name} </h3> <p> ${item.detail} </p></div></div>`;
      }else{
        html +=`<div class="message left"><img class="box" src="${picture}" /><div class="box"><h3> ${res.data.profile.display_name} </h3> <p> ${item.detail} </p> </div></div>`;
      }
    })
    $('.messages').append(html)
    startChat(token, topic)
  })
  $('#upload').change(function(){
    $('#frm').submit()
  })
  $('#frm').on('submit', uploadFiles)
  function uploadFiles(event){
    event.stopPropagation()
    event.preventDefault()
    $.ajax({
      url: "/api/v1/chat/savefile",
      type: 'POST',
      headers: {"Authorization": "Bearer "+token},
      data: new FormData(this),
      dataType: "JSON",
      processData: false,
      contentType: false,
    }).done(function(res) {
      $('#upload').val("")
      ws.getSubscription(topic).emit('message', {
        detail: `<a href="${res.file_name}" target="_blank"><img class="img_msg" src="${res.file_name}" /></a>`,
        display_name: display_name,
        picture: mypicture,
        topic: topic,
        uid: uid
      })
    })
    return
  }
  $('#message').keyup(function (e) {
    if (e.which === 13) {
      e.preventDefault()
      const message = $(this).val()
      $(this).val('')
      ws.getSubscription(topic).emit('message', {
        detail: message,
        display_name: display_name,
        picture: mypicture,
        topic: topic,
        uid: uid
      })
      return
    }
  })
})

function startChat (token, topic) {
  ws = adonis.Ws()
    .withApiToken(token)
    .connect()

  ws.on('open', () => {
    $('.connection-status').addClass('connected')
    subscribeToChannel(topic)
  })

  ws.on('error', () => {
    $('.connection-status').removeClass('connected')
  })
}
function subscribeToChannel (topic) {
  const chat = ws.subscribe(topic)

  chat.on('error', () => {
    $('.connection-status').removeClass('connected')
  })

  chat.on('message', (message) => {
    if(myuid == message.uid){
      $('.messages').append(`
        <div class="message left"><img class="box" src="${message.picture}" /><div class="box"><h3> ${message.display_name} </h3> <p> ${message.detail} </p></div></div>
      `)
    }else{
      $('.messages').append(`
        <div class="message right"><img class="box" src="${message.picture}" /><div class="box"><h3> ${message.display_name} </h3> <p> ${message.detail} </p> </div></div>
      `)
    }

  })
}
